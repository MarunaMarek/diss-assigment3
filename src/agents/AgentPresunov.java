package agents;

import OSPABA.*;
import OSPRNG.UniformContinuousRNG;
import OSPRNG.UniformDiscreteRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.HromadnyDopravnyProstriedok;
import simulation.model.Osoba;

import java.util.LinkedList;

//meta! id="12"
public class AgentPresunov extends Agent {
    private LinkedList<Osoba> osobnaPreprava;
    private LinkedList<HromadnyDopravnyProstriedok> hromadnaPreprava;
    private UniformDiscreteRNG nahodnyDopravnyProstriedok;
    private UniformContinuousRNG sancaNakazenia;
    private boolean ruska;
    private double ruskaEfektivita;

    public AgentPresunov(int id, Simulation mySim, Agent parent, int pocetDopravnychProstriedkov, boolean ruska, double ruskaEfektivita) {
        super(id, mySim, parent);
        init();
        this.ruska = ruska;
        this.ruskaEfektivita = ruskaEfektivita;
        addOwnMessage(Mc.koniecPresunu);
        osobnaPreprava = new LinkedList<>();
        hromadnaPreprava = new LinkedList<>();
        for (int i = 0; i < pocetDopravnychProstriedkov; i++) {
            hromadnaPreprava.add(new HromadnyDopravnyProstriedok());
        }
        nahodnyDopravnyProstriedok = new UniformDiscreteRNG(0, pocetDopravnychProstriedkov - 1);
        sancaNakazenia = new UniformContinuousRNG(0.0, 1.0);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        for (HromadnyDopravnyProstriedok p : hromadnaPreprava) {
            p.clear();
        }
        osobnaPreprava.clear();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerPresunov(Id.managerPresunov, mySim(), this);
        new PresunDopravnymProstriedkom(Id.presunDopravnymProstriedkom, mySim(), this);
        addOwnMessage(Mc.presunOsobu);
        addOwnMessage(Mc.vykonajKontrolu);
    }
    //meta! tag="end"

    public void pridajOsobuDoOsobnejPrepravy(Osoba osoba) {
        osobnaPreprava.add(osoba);
    }

    public void odoberOsobuZOsobnejPrepravy(Osoba osoba) {
        if (!osobnaPreprava.remove(osoba))
            System.out.println("Pokus o odobratie osoby ktorá nie je v osobnej preprave!");
    }

    public void pridajOsobuDoHromadnejPrepravy(Osoba osoba) {
        osoba.setDopravnyProstriedok(nahodnyDopravnyProstriedok.sample());
        hromadnaPreprava.get(osoba.getDopravnyProstriedok()).pridajOsobu(osoba);
    }

    public void odoberOsobuZHromadnejPrepravy(Osoba osoba) {
        hromadnaPreprava.get(osoba.getDopravnyProstriedok()).odoberOsobu(osoba);
        osoba.setDopravnyProstriedok(-1);
    }

    public LinkedList<HromadnyDopravnyProstriedok> getDopravneProstriedky() {
        return hromadnaPreprava;
    }

    public double sancaNakazenia() {
        return sancaNakazenia.sample();
    }

    public boolean isRuska() {
        return ruska;
    }

    public int pocetLudiVPreprave(){
        int vysledok = 0;
        for (HromadnyDopravnyProstriedok hr :hromadnaPreprava) {
            vysledok+= hr.size();
        }
        vysledok+=osobnaPreprava.size();
        return vysledok;
    }

    public double getRuskaEfektivita() {
        return ruskaEfektivita;
    }
}
