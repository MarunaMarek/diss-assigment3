package agents;

import OSPABA.*;
import OSPRNG.UniformContinuousRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.Osoba;
import simulation.model.Pracovisko;
import simulation.model.RezidencnaOblast;

import java.util.LinkedList;

//meta! id="13"
public class AgentRezidencii extends Agent {

    private final LinkedList<RezidencnaOblast> residencnaOblasti;
    private final LinkedList<Osoba> zamestnany;
    private final LinkedList<Osoba> deti;
    private final LinkedList<Osoba> nezamestnany;
    private final LinkedList<Osoba> stary;
    private final LinkedList<Osoba> osoby;
    private final LinkedList<Osoba> mrtvy;
    private final LinkedList<Osoba> karantena;
    private final UniformContinuousRNG sancaNezamestnanyOdchod;
    private final UniformContinuousRNG nahodnyOdchod;
    private final UniformContinuousRNG sancaStaryOdchod;
    private final UniformContinuousRNG sancaZamestnanyOdchod;
    private final UniformContinuousRNG sancaNakazenia;
    private final UniformContinuousRNG sancaTestovanie;
    private final int kolkoTestovat;
    private final boolean zakazStary;
    private final boolean zakazDeti;
    private final boolean zakazZamestnany;
    private final boolean zakazNezamestnany;

    public AgentRezidencii(int id, Simulation mySim, Agent parent, int pocetRezidencii, int kolkoTestovat, boolean zakazStary, boolean zakazDeti,
                           boolean zakazZamestnany, boolean zakazNezamestnany) {
        super(id, mySim, parent);
        init();
        this.zakazStary = zakazStary;
        this.zakazDeti = zakazDeti;
        this.zakazZamestnany = zakazZamestnany;
        this.zakazNezamestnany = zakazNezamestnany;
        this.kolkoTestovat = kolkoTestovat;
        residencnaOblasti = new LinkedList<>();
        for (int i = 0; i < pocetRezidencii; i++) {
            residencnaOblasti.add(new RezidencnaOblast());
        }
        zamestnany = new LinkedList<>();
        deti = new LinkedList<>();
        nezamestnany = new LinkedList<>();
        stary = new LinkedList<>();
        osoby = new LinkedList<>();
        mrtvy = new LinkedList<>();
        karantena = new LinkedList<>();
        addOwnMessage(Mc.koniecCakaniaDoDalsiehoDna);
        addOwnMessage(Mc.osobyVygenerovane);
        addOwnMessage(Mc.koniecRutiny);
        addOwnMessage(Mc.umiestniOsobu);
        addOwnMessage(Mc.otestujOsoby);
        sancaNezamestnanyOdchod = new UniformContinuousRNG(0.0, 1.0);
        sancaStaryOdchod = new UniformContinuousRNG(0.0, 1.0);
        sancaZamestnanyOdchod = new UniformContinuousRNG(0.0, 1.0);
        sancaNakazenia = new UniformContinuousRNG(0.0, 1.0);
        sancaTestovanie = new UniformContinuousRNG(0.0, 1.0);
        nahodnyOdchod = new UniformContinuousRNG(GlobalVariables.NAHODNY_ODCHOD_MIN, GlobalVariables.NAHODNY_ODCHOD_MAX);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        zamestnany.clear();
        deti.clear();
        nezamestnany.clear();
        stary.clear();
        osoby.clear();
        mrtvy.clear();
        karantena.clear();
        for (RezidencnaOblast r : residencnaOblasti) {
            r.clear();
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerRezidencii(Id.managerResidencii, mySim(), this);
        new CakajDoDalsiehoDna(Id.cakajDoDalsiehoDna, mySim(), this);
        new DennaRutina(Id.dennaRutina, mySim(), this);
        addOwnMessage(Mc.vykonajKontrolu);
        addOwnMessage(Mc.ubytujOsobu);
    }
    //meta! tag="end"

    public void pridajOsosbuDoRezidencie(Osoba osoba) {
        //System.out.println("Osoba pridana > " + ((Math.floor(mySim().currentTime() / GlobalVariables.DEN)) % 7) + " : " + (mySim().currentTime() % GlobalVariables.DEN) / GlobalVariables.HODINA);
        residencnaOblasti.get(osoba.getRezidencnaOblastMeno()).pridajOsobu(osoba);
    }

    public void odoberOsobuZRezidencie(Osoba osoba) {
        //System.out.println("Osoba odisla > " + ((Math.floor(mySim().currentTime() / GlobalVariables.DEN)) % 7) + " : " + (mySim().currentTime() % GlobalVariables.DEN) / GlobalVariables.HODINA);
        residencnaOblasti.get(osoba.getRezidencnaOblastMeno()).odoberOsobu(osoba);
    }

    public boolean najdiOsobu(Osoba osoba) {
        for (RezidencnaOblast rez : residencnaOblasti) {
            if (rez.getOsoby().contains(osoba)) return true;
        }
        return false;
    }

    public boolean nezamestnanyOdchod() {
        return sancaNezamestnanyOdchod.sample() < GlobalVariables.SANCA_DOSPELY_NEZAMESTNANY_ODCHOD;
    }

    public boolean staryOdchod() {
        return sancaStaryOdchod.sample() < GlobalVariables.SANCA_STARY_ODCHOD;
    }

    public boolean zamestnanyOdchod() {
        return sancaZamestnanyOdchod.sample() < GlobalVariables.SANCA_ZAMESTNANY_ODCHOD;
    }

    public double nahodnyOdchod() {
        return nahodnyOdchod.sample();
    }

    public void pridajOsobuDoKategorie(Osoba osoba) {
        switch (osoba.getTypOsoby()) {
            case STARY:
                stary.add(osoba);
                break;
            case DIETA:
                deti.add(osoba);
                break;
            case DOSPELY:
                if (osoba.jeZamestnany()) {
                    zamestnany.add(osoba);
                } else {
                    nezamestnany.add(osoba);
                }
                break;
        }
        osoby.add(osoba);
    }

    public LinkedList<Osoba> getZamestnany() {
        return zamestnany;
    }

    public LinkedList<Osoba> getDeti() {
        return deti;
    }

    public LinkedList<Osoba> getNezamestnany() {
        return nezamestnany;
    }

    public LinkedList<Osoba> getStary() {
        return stary;
    }

    public LinkedList<RezidencnaOblast> getRezidencneOblasti() {
        return residencnaOblasti;
    }

    public double sancaNakazenia() {
        return sancaNakazenia.sample();
    }

    public int getKolkoTestovat() {
        return kolkoTestovat;
    }

    public LinkedList<Osoba> getOsoby() {
        return osoby;
    }

    public double sancaTestovanie() {
        return sancaTestovanie.sample();
    }

    public LinkedList<Osoba> getMrtvy() {
        return mrtvy;
    }

    public LinkedList<Osoba> getKarantena() {
        return karantena;
    }

    public int pocetLudiDoma(){
        int vysledok = 0;
        for (RezidencnaOblast pr : residencnaOblasti) {
            vysledok += pr.size();
        }
        return vysledok;
    }

    public boolean isZakazStary() {
        return zakazStary;
    }

    public boolean isZakazDeti() {
        return zakazDeti;
    }

    public boolean isZakazZamestnany() {
        return zakazZamestnany;
    }

    public boolean isZakazNezamestnany() {
        return zakazNezamestnany;
    }
}
