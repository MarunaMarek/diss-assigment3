package agents;

import OSPABA.Agent;
import OSPABA.Simulation;
import OSPRNG.UniformContinuousRNG;
import OSPRNG.UniformDiscreteRNG;
import continualAssistants.NavstevaCentra;
import managers.ManagerCentrum;
import simulation.GlobalVariables;
import simulation.Id;
import simulation.Mc;
import simulation.model.Centrum;
import simulation.model.Osoba;
import simulation.model.Skola;

import java.util.LinkedList;

//meta! id="84"
public class AgentCentrum extends Agent {

	private final UniformContinuousRNG detiCasVCentre;
	private final UniformContinuousRNG dospelyCasVCentre;
    private final UniformContinuousRNG nazamestnanyCasVCentre;
    private final UniformContinuousRNG staryCasVCentre;
    private final UniformDiscreteRNG nahodneCentrum;
	private final UniformContinuousRNG sancaNakazenia;
    private final LinkedList<Centrum> centra;
    private boolean ruska;
    private double ruskaEfektivita;

    public AgentCentrum(int id, Simulation mySim, Agent parent, int pocetCentier, boolean ruska, double ruskaEfektivita) {
        super(id, mySim, parent);
        init();
        this.ruskaEfektivita = ruskaEfektivita;
        this.ruska = ruska;
        addOwnMessage(Mc.koniecCentra);
        detiCasVCentre = new UniformContinuousRNG(GlobalVariables.DETI_CENTRUM_MIN, GlobalVariables.DETI_CENTRUM_MAX);
        dospelyCasVCentre = new UniformContinuousRNG(GlobalVariables.DOSPELY_CENTRUM_MIN, GlobalVariables.DOSPELY_CENTRUM_MAX);
        nazamestnanyCasVCentre = new UniformContinuousRNG(GlobalVariables.NEZAMESTNANY_CENTRUM_MIN, GlobalVariables.NEZAMESTNANY_CENTRUM_MAX);
        staryCasVCentre = new UniformContinuousRNG(GlobalVariables.STARY_CENTRUM_MIN, GlobalVariables.STARY_CENTRUM_MAX);
        sancaNakazenia = new UniformContinuousRNG(0.0, 1.0);
        nahodneCentrum = new UniformDiscreteRNG(0, pocetCentier-1);
        centra = new LinkedList<>();
        for(int i = 0; i<pocetCentier; i++){
            centra.add(new Centrum());
        }
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        for(Centrum c : centra){
            c.clear();
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerCentrum(Id.managerCentrum, mySim(), this);
        new NavstevaCentra(Id.navstevaCentra, mySim(), this);
        addOwnMessage(Mc.umiestniCentrum);
        addOwnMessage(Mc.vykonajKontrolu);
    }
    //meta! tag="end"

	public double detiCasVCentre(){
    	return detiCasVCentre.sample();
	}

    public double dospelyCasVCentre(){
        return dospelyCasVCentre.sample();
    }

    public double nazamestnanyCasVCentre(){
        return nazamestnanyCasVCentre.sample();
    }

    public double staryCasVCentre(){
        return staryCasVCentre.sample();
    }

	public void pridajOsosbu(Osoba osoba){
        osoba.setDopravnyProstriedok(nahodneCentrum.sample());
		centra.get(osoba.getDopravnyProstriedok()).pridajOsobu(osoba);
	}

	public void odoberOsobu(Osoba osoba){
        centra.get(osoba.getDopravnyProstriedok()).odoberOsobu(osoba);
        osoba.setDopravnyProstriedok(-1);
	}

	public double sancaNakazenia(){
        return sancaNakazenia.sample();
    }

    public LinkedList<Centrum> getCentra() {
        return centra;
    }

    public int pocetLudiVCentrach(){
        int vysedok = 0;
        for(Centrum skola : centra){
            vysedok+=skola.size();
        }
        return vysedok;
    }

    public boolean isRuska() {
        return ruska;
    }

    public double getRuskaEfektivita() {
        return ruskaEfektivita;
    }
}
