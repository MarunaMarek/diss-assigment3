package agents;

import OSPABA.*;
import OSPRNG.TriangularRNG;
import OSPRNG.UniformContinuousRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.Osoba;

import java.util.LinkedList;

//meta! id="22"
public class AgentInfekcie extends Agent {

    private final TriangularRNG casVaznePriznaky;
    private final TriangularRNG casChoroba;
    private final UniformContinuousRNG sancaZotavenia;
    private final UniformContinuousRNG sancaNakaza;
    private final LinkedList<Osoba> infekcny;
    private final LinkedList<Osoba> testovany;
    private final LinkedList<Osoba> vNemocnici;
    private final LinkedList<Osoba> mrtvy;
    private final LinkedList<Osoba> vylieceny;
    private double casVrcholInfekcie;
    private double vrcholInfekcie;
    private double casVrcholKarantena;
    private double vrcholKarantena;
    private double casVrcholNemocnica;
    private double vrcholNemocnica;

    public AgentInfekcie(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
        addOwnMessage(Mc.koniecChoroby);
        casVaznePriznaky = new TriangularRNG(GlobalVariables.NAKAZA_DOBA_MIN, GlobalVariables.NAKAZA_DOBA_MID, GlobalVariables.NAKAZA_DOBA_MAX);
        casChoroba = new TriangularRNG(GlobalVariables.ZOTAVENIE_DOBA_MIN, GlobalVariables.ZOTAVENIE_DOBA_MID, GlobalVariables.ZOTAVENIE_DOBA_MAX);
        sancaZotavenia = new UniformContinuousRNG(0.0, 1.0);
        sancaNakaza = new UniformContinuousRNG(0.0, 1.0);
        infekcny = new LinkedList<>();
        testovany = new LinkedList<>();
        vNemocnici = new LinkedList<>();
        mrtvy = new LinkedList<>();
        vylieceny = new LinkedList<>();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        infekcny.clear();
        testovany.clear();
        vNemocnici.clear();
        mrtvy.clear();
        vylieceny.clear();
        casVrcholInfekcie = 0.0;
        vrcholInfekcie = 0.0;
        casVrcholKarantena = 0.0;
        vrcholKarantena = 0.0;
        casVrcholNemocnica = 0.0;
        vrcholNemocnica = 0.0;
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerInfekcie(Id.managerInfekcie, mySim(), this);
        new PriebehChoroby(Id.priebehChoroby, mySim(), this);
        new VaznePriznaky(Id.vaznePriznaky, mySim(), this);
        addOwnMessage(Mc.zmenaStavu);
    }
    //meta! tag="end"


    public double getCasVaznePriznaky() {
        return casVaznePriznaky.sample();
    }

    public double getCasChoroba() {
        return casChoroba.sample();
    }

    public double getSancaZotavenia() {
        return sancaZotavenia.sample();
    }

    public double getSancaNakaza() {
        return sancaNakaza.sample();
    }

    public void pridajInfekcneho(Osoba osoba) {
        //System.out.println("Infecny:" + infekcny.size());
        infekcny.add(osoba);
        if(infekcny.size()>vrcholInfekcie){
            vrcholInfekcie = infekcny.size();
            casVrcholInfekcie = mySim().currentTime();
        }
    }

    public void odoberInfekcneho(Osoba osoba) {
        if (!infekcny.remove(osoba)) System.out.println("Chyba pri odoberani infekcneho");
    }

    public void pridajTestovaneho(Osoba osoba) {
        //System.out.println("Testovany:" + testovany.size());
        testovany.add(osoba);
        if(testovany.size()>vrcholKarantena){
            vrcholKarantena = testovany.size();
            casVrcholKarantena = mySim().currentTime();
        }
    }

    public void odoberTestovaneho(Osoba osoba) {
        if (!testovany.remove(osoba)) System.out.println("Chyba pri odoberani testovaneho");
    }

    public void pridajDoNemocnice(Osoba osoba) {
        //System.out.println("Nemocnica:" + vNemocnici.size());
        vNemocnici.add(osoba);
        if(vNemocnici.size()>vrcholNemocnica){
            vrcholNemocnica = vNemocnici.size();
            casVrcholNemocnica = mySim().currentTime();
        }
    }

    public void odoberZNemocnice(Osoba osoba) {
        if (!vNemocnici.remove(osoba)) System.out.println("Chyba pri odoberani z nemocnice");
    }

    public boolean jeInfekcny(Osoba osoba) {
        return infekcny.contains(osoba);
    }

    public boolean jeVNemocnici(Osoba osoba) {
        return vNemocnici.contains(osoba);
    }

    public boolean jeTestovany(Osoba osoba) {
        return testovany.contains(osoba);
    }

    public void pridajUmrtie(Osoba osoba) {
        //System.out.println("Mrtvy:" + mrtvy.size());
        mrtvy.add(osoba);
    }

    public void pridajVylieceneho(Osoba osoba) {
        //System.out.println("Vylieceny:" + vylieceny.size());
        vylieceny.add(osoba);
    }

    public int pocetInfekcny() {
        return infekcny.size();
    }

    public int pocetTestovany() {
        return testovany.size();
    }

    public int pocetvNemocnici() {
        return vNemocnici.size();
    }

    public int pocetMrtvy() {
        return mrtvy.size();
    }

    public int pocetVylieceny() {
        return vylieceny.size();
    }

    public double[] getVrcholInfekcie() {
        return new double[]{vrcholInfekcie,casVrcholInfekcie};
    }

    public double[] getVrcholKarantena() {
        return new double[]{vrcholKarantena, casVrcholKarantena};
    }

    public double[] getVrcholNemocnica() {
            return new double[]{vrcholNemocnica, casVrcholNemocnica};
    }
}
