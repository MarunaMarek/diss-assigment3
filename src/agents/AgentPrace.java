package agents;

import OSPABA.*;
import OSPRNG.UniformContinuousRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.Osoba;
import simulation.model.Pracovisko;

import java.util.LinkedList;

//meta! id="82"
public class AgentPrace extends Agent {
    private final LinkedList<Pracovisko> pracoviska;
    private final UniformContinuousRNG casPrace;
    private final UniformContinuousRNG sancaNakazenia;
    private boolean ruska;
    private double ruskaEfektivita;

    public AgentPrace(int id, Simulation mySim, Agent parent, int pocetPracovisk, boolean ruska, double ruskaEfektivita) {
        super(id, mySim, parent);
        init();
        this.ruskaEfektivita = ruskaEfektivita;
        addOwnMessage(Mc.koniecPrace);
        this.ruska = ruska;
        pracoviska = new LinkedList<>();
        for (int i = 0; i< pocetPracovisk; i++){
            pracoviska.add(new Pracovisko());
        }
        casPrace = new UniformContinuousRNG(GlobalVariables.CAS_PRACE_MIN, GlobalVariables.CAS_PRACE_MAX);
        sancaNakazenia = new UniformContinuousRNG(0.0, 1.0);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        for(Pracovisko pracovisko : pracoviska){
            pracovisko.clear();
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerPrace(Id.managerPrace, mySim(), this);
        new NavstevaPrace(Id.navstevaPrace, mySim(), this);
        addOwnMessage(Mc.umiestniPraca);
        addOwnMessage(Mc.vykonajKontrolu);
    }
    //meta! tag="end"

    public void pridajOsobu(Osoba osoba){
        pracoviska.get(osoba.getPracovisko()).pridajOsobu(osoba);
    }

    public void odoberOsobu(Osoba osoba){
        pracoviska.get(osoba.getPracovisko()).odoberOsobu(osoba);
    }

    public LinkedList<Pracovisko> getPracoviska() {
        return pracoviska;
    }

    public double sancaNakazenia(){
        return sancaNakazenia.sample();
    }

    public double casPrace(){
        return casPrace.sample();
    }

    public int pocetLudiVPraci(){
        int vysledok = 0;
        for (Pracovisko pr : pracoviska) {
            vysledok += pr.size();
        }
        return vysledok;
    }

    public boolean isRuska() {
        return ruska;
    }

    public double getRuskaEfektivita() {
        return ruskaEfektivita;
    }
}
