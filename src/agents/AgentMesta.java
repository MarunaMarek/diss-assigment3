package agents;

import OSPABA.*;
import simulation.*;
import managers.*;
import continualAssistants.*;

//meta! id="3"
public class AgentMesta extends Agent {
    public AgentMesta(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
        addOwnMessage(Mc.osobyVygenerovane);
        addOwnMessage(Mc.otestujOsoby);
        addOwnMessage(Mc.koniecTestovania);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerMesta(Id.managerMesta, mySim(), this);
        addOwnMessage(Mc.umiestniOsobu);
        addOwnMessage(Mc.generujOsoby);
        addOwnMessage(Mc.presunOsobu);
        addOwnMessage(Mc.vykonajKontrolu);
        addOwnMessage(Mc.ubytujOsobu);
        addOwnMessage(Mc.koniecKontroly);
    }
    //meta! tag="end"
}
