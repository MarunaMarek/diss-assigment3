package agents;

import OSPABA.*;
import OSPRNG.UniformContinuousRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;

//meta! id="4"
public class AgentMiest extends Agent {

	private final UniformContinuousRNG nemocnicaGenerator;
	private final UniformContinuousRNG dietaDoNakupnehoCentra;
	private final UniformContinuousRNG dospelyDoNakupnehoCentra;

    public AgentMiest(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
        nemocnicaGenerator = new UniformContinuousRNG(0.0,1.0);
        dietaDoNakupnehoCentra = new UniformContinuousRNG(0.0,1.0);
        dospelyDoNakupnehoCentra = new UniformContinuousRNG(0.0,1.0);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerMiest(Id.managerMiest, mySim(), this);
        addOwnMessage(Mc.koniecUmiestnenia);
        addOwnMessage(Mc.umiestniOsobu);
        addOwnMessage(Mc.koniecKontroly);
        addOwnMessage(Mc.vykonajKontrolu);
    }
    //meta! tag="end"

	public boolean ideDoNemocnice(){
		return nemocnicaGenerator.sample() < 0.5;
	}

	public boolean ideDietaDoCentra(){
        return dietaDoNakupnehoCentra.sample() < GlobalVariables.SANCA_DETI_CENTRUM;
    }

    public boolean ideDospelyDoCentra(){
        return dospelyDoNakupnehoCentra.sample() < GlobalVariables.SANCA_DOSPELY_CENTRUM;
    }
}
