package agents;

import OSPABA.*;
import simulation.*;
import managers.*;
import continualAssistants.*;

//meta! id="5"
public class AgentZdravia extends Agent {
    public AgentZdravia(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
        addOwnMessage(Mc.koniecKontrolyZdravia);
        addOwnMessage(Mc.zacniKontrolovat);
        addOwnMessage(Mc.koniecTestovania);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerZdravia(Id.managerZdravia, mySim(), this);
        new Kontrola(Id.kontrola, mySim(), this);
        new Testovanie(Id.testovanie, mySim(), this);
        addOwnMessage(Mc.koniecKontroly);
        addOwnMessage(Mc.zmenaStavu);
    }
    //meta! tag="end"
}
