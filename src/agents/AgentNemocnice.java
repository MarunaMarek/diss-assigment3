package agents;

import OSPABA.*;
import OSPRNG.UniformContinuousRNG;
import OSPRNG.UniformDiscreteRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.Osoba;
import simulation.model.Skola;
import simulation.model.ZdravotnickeZariadenie;

import java.util.LinkedList;

//meta! id="85"
public class AgentNemocnice extends Agent {
	private final UniformContinuousRNG nazamestnanyCasVNemocnici;
	private final UniformContinuousRNG staryCasVNemocnici;
	private final UniformDiscreteRNG nahodnaNemocnica;
	private final UniformContinuousRNG sancaNakazenia;
	private final LinkedList<ZdravotnickeZariadenie> nemocnice;
	private boolean ruska;
	private double ruskaEfektivita;

    public AgentNemocnice(int id, Simulation mySim, Agent parent, int pocetZdravotnickychZariadeni, boolean ruska, double ruskaEfektivita) {
        super(id, mySim, parent);
        init();
        this.ruskaEfektivita = ruskaEfektivita;
        this.ruska = ruska;
		addOwnMessage(Mc.koniecNemocnice);
		nazamestnanyCasVNemocnici = new UniformContinuousRNG(GlobalVariables.NEZAMESTNANY_NEMOCNICA_MIN, GlobalVariables.NEZAMESTNANY_NEMOCNICA_MAX);
		staryCasVNemocnici = new UniformContinuousRNG(GlobalVariables.STARY_NEMOCNICA_MIN, GlobalVariables.STARY_NEMOCNICA_MAX);
		sancaNakazenia = new UniformContinuousRNG(0.0, 1.0);
		nahodnaNemocnica = new UniformDiscreteRNG(0, pocetZdravotnickychZariadeni-1);
		nemocnice = new LinkedList<>();
		for (int i = 0; i < pocetZdravotnickychZariadeni; i++) {
			nemocnice.add(new ZdravotnickeZariadenie());
		}
	}

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
		for(ZdravotnickeZariadenie z : nemocnice){
			z.clear();
		}
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerNemocnice(Id.managerNemocnice, mySim(), this);
        new NavstevaNemocnice(Id.navstevaNemocnice, mySim(), this);
        addOwnMessage(Mc.umiestniNemocnica);
        addOwnMessage(Mc.vykonajKontrolu);
    }

    public void pridajOsobuDoNemocnice(Osoba osoba){
    	osoba.setDopravnyProstriedok(nahodnaNemocnica.sample());
    	nemocnice.get(osoba.getDopravnyProstriedok()).pridajOsobu(osoba);
	}

	public void odoberOsobuZNemocnice(Osoba osoba){
		nemocnice.get(osoba.getDopravnyProstriedok()).odoberOsobu(osoba);
		osoba.setDopravnyProstriedok(-1);
	}

    //meta! tag="end"
	public double nazamestnanyCasVNemocnici(){
		return nazamestnanyCasVNemocnici.sample();
	}

	public double staryCasVNemocnici(){
		return staryCasVNemocnici.sample();
	}

	public double sancaNakazenia(){
    	return sancaNakazenia.sample();
	}

	public LinkedList<ZdravotnickeZariadenie> getNemocnice() {
		return nemocnice;
	}

	public int pocetLudiVZariadeniach(){
		int vysedok = 0;
		for(ZdravotnickeZariadenie skola : nemocnice){
			vysedok+=skola.size();
		}
		return vysedok;
	}

	public boolean isRuska() {
		return ruska;
	}

	public double getRuskaEfektivita() {
		return ruskaEfektivita;
	}
}
