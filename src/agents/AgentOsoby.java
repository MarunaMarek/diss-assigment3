package agents;

import OSPABA.*;
import OSPDataStruct.SimStack;
import OSPRNG.UniformContinuousRNG;
import OSPRNG.UniformDiscreteRNG;
import simulation.*;
import managers.*;
import simulation.model.Osoba;

//meta! id="11"
public class AgentOsoby extends Agent {
    private final UniformContinuousRNG casDochadzaniaDeti;
    private final UniformContinuousRNG sancaDospelyHromadnaDoprava;
    private final UniformContinuousRNG sancaNezamestnanyHromadnaDoprava;
    private final UniformContinuousRNG casDochadzaniaDospelyHromadne;
    private final UniformContinuousRNG casDochadzaniaDospelyIndividualne;
    private final UniformContinuousRNG casDochadzaniaNezamesnany;
    private final UniformContinuousRNG sancaDospelyNezamestnany;
    private final UniformContinuousRNG casDochadzaniaStary;
    private final UniformContinuousRNG sancaStaryHromadnaDoprava;
    private final UniformContinuousRNG sancaNakazeny;
    private final UniformDiscreteRNG rezidencnaOblast;
    private final UniformDiscreteRNG nahodnaSkola;
    private final UniformDiscreteRNG nahodnaPraca;
    private int pocetDeti;
    private int pocetDospely;
    private int pocetStary;

    public AgentOsoby(int id, Simulation mySim, Agent parent, int pocetPracovisk, int pocetRezidencii, int pocetSkol, int pocetDospely, int pocetStary, int pocetDeti) {
        super(id, mySim, parent);
        init();
        this.pocetDeti = pocetDeti;
        this.pocetDospely = pocetDospely;
        this.pocetStary = pocetStary;
        casDochadzaniaDeti = new UniformContinuousRNG(GlobalVariables.CESTA_DETI_MIN, GlobalVariables.CESTA_DETI_MAX);
        casDochadzaniaDospelyHromadne = new UniformContinuousRNG(GlobalVariables.CESTA_DOSPELY_HROMADNE_MIN, GlobalVariables.CESTA_DOSPELY_HROMADNE_MAX);
        casDochadzaniaDospelyIndividualne = new UniformContinuousRNG(GlobalVariables.CESTA_DOSPELY_INDIVIDUALNE_MIN, GlobalVariables.CESTA_DOSPELY_INDIVIDUALNE_MAX);
        casDochadzaniaNezamesnany = new UniformContinuousRNG(GlobalVariables.CESTA_NEZAMESTNANY_MIN, GlobalVariables.CESTA_NEZAMESTNANY_MAX);
        sancaDospelyHromadnaDoprava = new UniformContinuousRNG(0.0,1.0);
        sancaNezamestnanyHromadnaDoprava = new UniformContinuousRNG(0.0,1.0);
        sancaDospelyNezamestnany = new UniformContinuousRNG(0.0,1.0);
        sancaNakazeny = new UniformContinuousRNG(0.0,1.0);
        rezidencnaOblast = new UniformDiscreteRNG(0, pocetRezidencii-1);
        nahodnaSkola = new UniformDiscreteRNG(0, pocetSkol-1);
        nahodnaPraca = new UniformDiscreteRNG(0, pocetPracovisk-1);
        casDochadzaniaStary = new UniformContinuousRNG(GlobalVariables.CESTA_STARY_MIN, GlobalVariables.CESTA_STARY_MAX);
        sancaStaryHromadnaDoprava = new UniformContinuousRNG(0.0, 1.0);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerOsoby(Id.managerOsoby, mySim(), this);
        addOwnMessage(Mc.generujOsoby);
        addOwnMessage(Mc.vykonajKontrolu);
    }
    //meta! tag="end"

    public double casDochadzaniaDeti() {
        return casDochadzaniaDeti.sample();
    }

    public int rezidencnaOblast() {
        return rezidencnaOblast.sample();
    }

    public boolean cestujeDospelyHromadne(){
        return sancaDospelyHromadnaDoprava.sample() < GlobalVariables.SANCA_DOSPELY_HROMADNE;
    }

    public boolean cestujeNezamestnanyHromadne(){
        return sancaNezamestnanyHromadnaDoprava.sample() < GlobalVariables.SANCA_NEZAMESTNANY_HROMADNE;
    }

    public boolean jeDospelyZamestnany(){
        return sancaDospelyNezamestnany.sample() < GlobalVariables.SANCA_DOSPELY_ZAMESTNANY;
    }

    public double casDochadzaniaDospelyHromadne(){
        return casDochadzaniaDospelyHromadne.sample();
    }

    public double casDochadzaniaDospelyIndividualne(){
        return casDochadzaniaDospelyIndividualne.sample();
    }

    public double casDochadzaniaNezamestnany(){
        return casDochadzaniaNezamesnany.sample();
    }

    public boolean cestujeStaryHromadne(){
        return sancaStaryHromadnaDoprava.sample() < GlobalVariables.SANCA_STARY_HROMADNE;
    }

    public double casDochadzaniaStary(){
        return casDochadzaniaStary.sample();
    }

    public int nahodnaSkola(){
        return nahodnaSkola.sample();
    }

    public int nahodnaPraca(){
        return nahodnaPraca.sample();
    }

    public double sancaNakazeny(){
        return sancaNakazeny.sample();
    }

    public int getPocetDeti() {
        return pocetDeti;
    }

    public int getPocetDospely() {
        return pocetDospely;
    }

    public int getPocetStary() {
        return pocetStary;
    }

    public int getPocetLudi(){
        return pocetDospely+pocetStary+pocetDeti;
    }
}
