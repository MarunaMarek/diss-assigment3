package agents;

import OSPABA.*;
import OSPRNG.TriangularRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.Osoba;

import java.util.LinkedList;

//meta! id="23"
public class AgentNakazeny extends Agent {
	private TriangularRNG inkubacnaDoba;
	private LinkedList<Osoba> nakazeny;

    public AgentNakazeny(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        init();
        inkubacnaDoba = new TriangularRNG(GlobalVariables.INKUBACNA_DOBA_MIN, GlobalVariables.INKUBACNA_DOBA_MID, GlobalVariables.INKUBACNA_DOBA_MAX);
        addOwnMessage(Mc.koniecInkubacnejDoby);
        nakazeny = new LinkedList<>();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        nakazeny.clear();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerNakazeny(Id.managerNakazeny, mySim(), this);
        new InkubacnaDoba(Id.inkubacnaDoba, mySim(), this);
        addOwnMessage(Mc.koniecKontroly);
    }
    //meta! tag="end"

	public double getInkubacnaDoba(){
    	return inkubacnaDoba.sample();
	}

	public void pridajNakazeneho(Osoba osoba){
        nakazeny.add(osoba);
    }

    public void odoberNakazeneho(Osoba osoba){
        if(!nakazeny.remove(osoba)) System.out.println("Chyba pri odoberani nakazeneho");;
    }

    public int pocetNakazenych(){
        return nakazeny.size();
    }
}
