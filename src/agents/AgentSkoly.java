package agents;

import OSPABA.*;
import OSPRNG.UniformContinuousRNG;
import simulation.*;
import managers.*;
import continualAssistants.*;
import simulation.model.Osoba;
import simulation.model.Skola;

import java.util.LinkedList;

//meta! id="83"
public class AgentSkoly extends Agent {
    private final UniformContinuousRNG sancaNakazenia;
    private final LinkedList<Skola> skoly;
    private boolean ruska;
    private double ruskaEfektivita;

    public AgentSkoly(int id, Simulation mySim, Agent parent, int pocetSkol, boolean ruska, double ruskaEfektivita) {
        super(id, mySim, parent);
        init();
        this.ruskaEfektivita = ruskaEfektivita;
        this.ruska = ruska;
		addOwnMessage(Mc.koniecVyucovania);
		sancaNakazenia = new UniformContinuousRNG(0.0, 1.0);
		skoly = new LinkedList<>();
		for(int i = 0; i< pocetSkol; i++){
		    skoly.add(new Skola());
        }
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
        for(Skola skola : skoly){
            skola.clear();
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init() {
        new ManagerSkoly(Id.managerSkoly, mySim(), this);
        new NavstevaSkoly(Id.navstevaSkoly, mySim(), this);
        addOwnMessage(Mc.umiestniSkola);
        addOwnMessage(Mc.vykonajKontrolu);
    }
    //meta! tag="end"

    public void pridajOsobu(Osoba osoba){
        skoly.get(osoba.getSkola()).pridajOsobu(osoba);
    }

    public void odoberOsobu(Osoba osoba){
        skoly.get(osoba.getSkola()).odoberOsobu(osoba);
    }

    public double sancaNakazenia() {
        return sancaNakazenia.sample();
    }

    public LinkedList<Skola> getSkoly() {
        return skoly;
    }

    public int pocetLudiVSkolach(){
        int vysedok = 0;
        for(Skola skola : skoly){
            vysedok+=skola.size();
        }
        return vysedok;
    }

    public boolean isRuska() {
        return ruska;
    }

    public double getRuskaEfektivita() {
        return ruskaEfektivita;
    }
}
