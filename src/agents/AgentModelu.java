package agents;

import OSPABA.*;
import simulation.*;
import managers.*;
import continualAssistants.*;

//meta! id="1"
public class AgentModelu extends Agent
{
	public AgentModelu(int id, Simulation mySim, Agent parent)
	{
		super(id, mySim, parent);
		init();
		addOwnMessage(Mc.generujOsoby);
		addOwnMessage(Mc.otestujOsoby);
		addOwnMessage(Mc.koniecTestovania);
	}

	@Override
	public void prepareReplication()
	{
		super.prepareReplication();
		// Setup component for the next replication
	}

	//meta! userInfo="Generated code: do not modify", tag="begin"
	private void init()
	{
		new ManagerModelu(Id.managerModelu, mySim(), this);
		addOwnMessage(Mc.vykonajKontrolu);
		addOwnMessage(Mc.koniecKontroly);
	}
	//meta! tag="end"

	public void spustiSimulaciu(){
		MyMessage message = new MyMessage(mySim());
		message.setCode(Mc.generujOsoby);
		message.setAddressee(this);
		manager().notice(message);
	}
}
