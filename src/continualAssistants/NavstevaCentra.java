package continualAssistants;

import OSPABA.*;
import simulation.*;
import agents.*;
import OSPABA.Process;
import simulation.model.Osoba;
import simulation.model.TypOsoby;

//meta! id="125"
public class NavstevaCentra extends Process {
    public NavstevaCentra(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentCentrum", id="126", type="Start"
    public void processStart(MessageForm message) {
		message.setCode(Mc.koniecCentra);
		Osoba osoba = ((MyMessage)message).getOsoba();
		double cas = 0.0;
		if(osoba.getTypOsoby() == TypOsoby.DIETA) {
            cas = myAgent().detiCasVCentre();
        }else if(osoba.getTypOsoby()== TypOsoby.DOSPELY && osoba.jeZamestnany() && ((mySim().currentTime() % GlobalVariables.DEN) < (20*GlobalVariables.HODINA))){
		    cas = GlobalVariables.HODINA;
        }else if(osoba.getTypOsoby()== TypOsoby.DOSPELY && osoba.jeZamestnany()){
            cas = myAgent().dospelyCasVCentre();
        }else if(osoba.getTypOsoby()== TypOsoby.DOSPELY && !osoba.jeZamestnany()){
            cas = myAgent().nazamestnanyCasVCentre();
        }else if(osoba.getTypOsoby()==TypOsoby.STARY){
		    cas = myAgent().staryCasVCentre();
        }
		hold(cas, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecCentra:
                assistantFinished(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentCentrum myAgent() {
        return (AgentCentrum) super.myAgent();
    }

}
