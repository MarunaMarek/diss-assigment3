package continualAssistants;

import OSPABA.*;
import simulation.*;
import agents.*;
import OSPABA.Process;

//meta! id="133"
public class InkubacnaDoba extends Process {
    public InkubacnaDoba(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentNakazeny", id="134", type="Start"
    public void processStart(MessageForm message) {
		message.setCode(Mc.koniecInkubacnejDoby);
		hold(myAgent().getInkubacnaDoba(), message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
			case Mc.koniecInkubacnejDoby:
				assistantFinished(message);
				break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentNakazeny myAgent() {
        return (AgentNakazeny) super.myAgent();
    }

}
