package continualAssistants;

import OSPABA.*;
import simulation.*;
import agents.*;
import OSPABA.Process;

//meta! id="119"
public class NavstevaSkoly extends Process {
    public NavstevaSkoly(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentSkoly", id="120", type="Start"
    public void processStart(MessageForm message) {
    	message.setCode(Mc.koniecVyucovania);
    	hold(GlobalVariables.DETI_SKOLA_MAX, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
			case Mc.koniecVyucovania:
				assistantFinished(message);
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentSkoly myAgent() {
        return (AgentSkoly) super.myAgent();
    }

}
