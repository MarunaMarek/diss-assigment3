package continualAssistants;

import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="128"
public class Testovanie extends Scheduler {
    public Testovanie(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentZdravia", id="129", type="Start"
    public void processStart(MessageForm message) {
        message.setCode(Mc.koniecKontrolyZdravia);
        double time;
        if(mySim().currentTime()==0.0){
            time = (3*GlobalVariables.HODINA)+(45*GlobalVariables.MINUTA);
        }else {
            time = 24*GlobalVariables.HODINA;
        }
        hold(time, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecKontrolyZdravia:
                assistantFinished(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentZdravia myAgent() {
        return (AgentZdravia) super.myAgent();
    }

}
