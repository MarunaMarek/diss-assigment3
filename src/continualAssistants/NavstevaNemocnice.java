package continualAssistants;

import OSPABA.*;
import simulation.*;
import agents.*;
import OSPABA.Process;
import simulation.model.Osoba;
import simulation.model.TypOsoby;

//meta! id="122"
public class NavstevaNemocnice extends Process {
    public NavstevaNemocnice(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentNemocnice", id="123", type="Start"
    public void processStart(MessageForm message) {
		message.setCode(Mc.koniecNemocnice);
		Osoba osoba = ((MyMessage)message).getOsoba();
		double cas = 0.0;
		if(osoba.getTypOsoby() == TypOsoby.DOSPELY) {
			cas = myAgent().nazamestnanyCasVNemocnici();
		}else {
			cas = myAgent().staryCasVNemocnici();
		}
		hold(cas, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
			case Mc.koniecNemocnice:
				assistantFinished(message);
				break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentNemocnice myAgent() {
        return (AgentNemocnice) super.myAgent();
    }

}
