package continualAssistants;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Scheduler;
import OSPABA.Simulation;
import agents.AgentZdravia;
import simulation.Mc;
import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="128"
public class DennaRutina extends Scheduler {
    public DennaRutina(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentRezidencii", id="129", type="Start"
    public void processStart(MessageForm message) {
        message.setCode(Mc.koniecRutiny);
        hold(24*GlobalVariables.HODINA, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecRutiny:
                assistantFinished(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentRezidencii myAgent() {
        return (AgentRezidencii) super.myAgent();
    }

}