package continualAssistants;

import OSPABA.*;
import simulation.*;
import agents.*;
import OSPABA.Process;
import simulation.model.Osoba;
import simulation.model.TypOsoby;

//meta! id="108"
public class CakajDoDalsiehoDna extends Process {
    public CakajDoDalsiehoDna(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="AgentResidencii", id="109", type="Start"
    public void processStart(MessageForm message) {
        MyMessage myMessage = (MyMessage) message;
        Osoba osoba = myMessage.getOsoba();
        myMessage.setCode(Mc.koniecCakaniaDoDalsiehoDna);
        boolean posliSpravu = true;
        double cas = 0.0;
        if (osoba.getTypOsoby() == TypOsoby.DIETA) {
            if ((Math.floor(mySim().currentTime() / GlobalVariables.DEN) % 7) == 5) {
                cas = 2 * GlobalVariables.DEN;
            }
            cas += (GlobalVariables.DEN - (mySim().currentTime() % GlobalVariables.DEN) + osoba.getOdchodZDomu());
        } else if (osoba.getTypOsoby() == TypOsoby.DOSPELY && osoba.jeZamestnany()) {
            double pomCas = ((mySim().currentTime() % GlobalVariables.DEN) / GlobalVariables.HODINA);
            double pomDen = (Math.floor(mySim().currentTime() / GlobalVariables.DEN) % 7);
            if ((pomCas > 0.0 && pomCas < 7.0) && (pomDen != 0 && pomDen != 6)) {
                cas = osoba.getOdchodZDomu() - (mySim().currentTime() % GlobalVariables.DEN);
            } else if (pomCas > 20.0 && pomCas <= 24.00) {
                if (pomDen != 5 && pomDen != 6) {
                    cas = (GlobalVariables.DEN - (mySim().currentTime() % GlobalVariables.DEN) + osoba.getOdchodZDomu());
                }else {
                    posliSpravu = false;
                }
            } else if (pomCas <= 20.0) {
                if (myAgent().zamestnanyOdchod()) {
                    if(myAgent().najdiOsobu(osoba)) {
                        cas = ((GlobalVariables.HODINA * 20) - (mySim().currentTime() % GlobalVariables.DEN));
                    }else {
                        posliSpravu = false;
                    }
                } else {
                    if (pomDen != 5 && pomDen != 6) {
                        if(myAgent().najdiOsobu(osoba)) {
                            cas = (GlobalVariables.DEN - (mySim().currentTime() % GlobalVariables.DEN) + osoba.getOdchodZDomu());
                        }else {
                            posliSpravu = false;
                        }
                    } else {
                        posliSpravu = false;
                    }
                }
            }else {
                posliSpravu = false;
            }
        } else if (osoba.getTypOsoby() == TypOsoby.STARY || (osoba.getTypOsoby() == TypOsoby.DOSPELY && !osoba.jeZamestnany())) {
            cas = (osoba.getOdchodZDomu() + myAgent().nahodnyOdchod());
        }
        if (posliSpravu)
            hold(cas, myMessage);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecCakaniaDoDalsiehoDna:
                assistantFinished(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.start:
                processStart(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentRezidencii myAgent() {
        return (AgentRezidencii) super.myAgent();
    }

}
