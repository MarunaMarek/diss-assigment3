package sample;

import simulation.GlobalVariables;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeConverter {
    private static Date date = new Date();
    public static String convertToDate(double time){
        date.setTime((long)(time* 1000));
        date.setHours(date.getHours()-1);
        String res = new SimpleDateFormat("HH:mm:ss").format(date);
        int days = (int)(time/ GlobalVariables.DEN);
        res = days + " " + res;
        return res;
    }
}
