package sample;

import OSPABA.ISimDelegate;
import OSPABA.SimState;
import OSPABA.Simulation;
import OSPStat.Stat;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import simulation.GlobalVariables;
import simulation.VirusSimulation;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements ISimDelegate, Initializable {
    @FXML
    public Button runStopButton;
    @FXML
    public ProgressBar replicationProgress;
    @FXML
    public Label cas;
    @FXML
    public LineChart<String, Number> nemocnicaGraf;
    @FXML
    public LineChart<String, Number> karantenaGraf;
    @FXML
    public LineChart<String, Number> vyliecenyGraf;
    @FXML
    public LineChart<String, Number> infeklcnyGraf;
    @FXML
    public LineChart<String, Number> nakazenyGraf;
    @FXML
    public LineChart<String, Number> mrtvyGraf;
    @FXML
    public TextArea infoPoctyZdravie;
    @FXML
    public TextArea infoPoctyMiesta;
    @FXML
    public CheckBox ruska;
    @FXML
    public CheckBox zakazZamestnany;
    @FXML
    public CheckBox zakazNezamestnany;
    @FXML
    public CheckBox zakazDeti;
    @FXML
    public CheckBox zakazStary;
    @FXML
    public CheckBox turbo;
    @FXML
    public TextField praceP;
    @FXML
    public TextField skolyP;
    @FXML
    public TextField centraP;
    @FXML
    public TextField strediskaP;
    @FXML
    public TextField mhdP;
    @FXML
    public TextField pocetLudi;
    @FXML
    public Slider rychlost;
    @FXML
    public TextField pocetReplikacii;
    @FXML
    public Button pauseButton;
    @FXML
    public TextField testyP;
    @FXML
    public TextField oblatiP;
    @FXML
    public TextArea infoVysledky;
    @FXML
    public TextField ruskaP;
    @FXML
    public Label aktualnaReplikccia;

    private boolean running;
    private boolean paused;
    private VirusSimulation virusSimulation;
    private Thread thread;
    private XYChart.Series<String, Number> nemocnicaSeries;
    private XYChart.Series<String, Number> karantenaSeries;
    private XYChart.Series<String, Number> vyliecenySeries;
    private XYChart.Series<String, Number> infeklcnySeries;
    private XYChart.Series<String, Number> nakazenySeries;
    private XYChart.Series<String, Number> mrtvySeries;

    private Stat dniPandemie;
    private Stat priemVylieceny;
    private Stat priemMrtvy;
    private Stat vrcholInfekcie;
    private Stat casVrcholInfekcie;
    private Stat vrcholKarantena;
    private Stat casVrcholKarantena;
    private Stat vrcholNemocnica;
    private Stat casVrcholNemocnica;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        turbo.setSelected(false);
        dniPandemie = new Stat();
        priemVylieceny = new Stat();
        priemMrtvy = new Stat();
        vrcholInfekcie = new Stat();
        casVrcholInfekcie = new Stat();
        vrcholKarantena = new Stat();
        casVrcholKarantena = new Stat();
        vrcholNemocnica = new Stat();
        casVrcholNemocnica = new Stat();
        running = false;
        paused = false;
        rychlost.valueProperty().addListener(((observable, oldValue, newValue) -> zmenaRychlosti(newValue.intValue())));
        setUpCharts();
    }

    public void runButtonPressed() {
        if (!running) {
            dniPandemie.clear();
            priemVylieceny.clear();
            priemMrtvy.clear();
            vrcholInfekcie.clear();
            casVrcholInfekcie.clear();
            vrcholKarantena.clear();
            casVrcholKarantena.clear();
            vrcholNemocnica.clear();
            casVrcholNemocnica.clear();
            double pLudi = Double.parseDouble(pocetLudi.getText());
            virusSimulation = new VirusSimulation((int) (pLudi * 0.62), (int) (pLudi * 0.19), (int) (pLudi * 0.19), Integer.parseInt(praceP.getText()),
                    Integer.parseInt(oblatiP.getText()), Integer.parseInt(skolyP.getText()), Integer.parseInt(mhdP.getText()),
                    Integer.parseInt(testyP.getText()), Integer.parseInt(centraP.getText()), Integer.parseInt(strediskaP.getText()),
                    ruska.isSelected(), zakazStary.isSelected(), zakazDeti.isSelected(), zakazZamestnany.isSelected(), zakazNezamestnany.isSelected(), Double.parseDouble(ruskaP.getText()));
            virusSimulation.onSimulationWillStart(this::onSimulationWillStart);
            virusSimulation.onReplicationWillStart(this::onReplicationWillStart);
            virusSimulation.onReplicationDidFinish(this::onReplicationDidFinish);
            virusSimulation.onSimulationDidFinish(this::onSimulationDidFinish);
            virusSimulation.onRefreshUI(this::refresh);
            virusSimulation.registerDelegate(this);
            thread = new Thread(() -> virusSimulation.simulate(Integer.parseInt(pocetReplikacii.getText()), 432000000));
            thread.setDaemon(true);
            thread.start();
            running = true;
            runStopButton.setText("Zastav");
        } else {
            virusSimulation.stopSimulation();
            running = false;
            runStopButton.setText("Spusti");
        }

    }

    public void pauseButtonPressed() {
        if (paused) {
            pauseButton.setText("Pauza");
            virusSimulation.resumeSimulation();
        } else {
            pauseButton.setText("Pokračuj");
            virusSimulation.pauseSimulation();
        }
        paused = !paused;
    }

    private void setUpCharts() {
        nakazenySeries = new XYChart.Series<>();
        nakazenyGraf.getData().clear();
        nakazenyGraf.getData().add(nakazenySeries);
        nakazenyGraf.setCreateSymbols(false);

        nemocnicaSeries = new XYChart.Series<>();
        nemocnicaGraf.getData().clear();
        nemocnicaGraf.getData().add(nemocnicaSeries);
        nemocnicaGraf.setCreateSymbols(false);

        karantenaSeries = new XYChart.Series<>();
        karantenaGraf.getData().clear();
        karantenaGraf.getData().add(karantenaSeries);
        karantenaGraf.setCreateSymbols(false);

        vyliecenySeries = new XYChart.Series<>();
        vyliecenyGraf.getData().clear();
        vyliecenyGraf.getData().add(vyliecenySeries);
        vyliecenyGraf.setCreateSymbols(false);

        infeklcnySeries = new XYChart.Series<>();
        infeklcnyGraf.getData().clear();
        infeklcnyGraf.getData().add(infeklcnySeries);
        infeklcnyGraf.setCreateSymbols(false);

        mrtvySeries = new XYChart.Series<>();
        mrtvyGraf.getData().clear();
        mrtvyGraf.getData().add(mrtvySeries);
        mrtvyGraf.setCreateSymbols(false);
    }

    @Override
    public void simStateChanged(Simulation simulation, SimState simState) {
        switch (simState) {
            case running:
                onSimulationWillStart(simulation);
                break;
            case replicationRunning:
                break;
            case replicationStopped:
                break;
            case stopped:
                onSimulationDidFinish(simulation);
                break;
        }
    }

    @Override
    public void refresh(Simulation simulation) {
        Platform.runLater(() -> {
            VirusSimulation virusSimulation = (VirusSimulation) simulation;
            int mrtvyP = virusSimulation.agentInfekcie().pocetMrtvy();
            int vyliecenyP = virusSimulation.agentInfekcie().pocetVylieceny();
            int nakazenyP = virusSimulation.agentNakazeny().pocetNakazenych();
            int infekcnyP = virusSimulation.agentInfekcie().pocetInfekcny();
            int testovanyP = virusSimulation.agentInfekcie().pocetTestovany();
            int nemocnicaP = virusSimulation.agentInfekcie().pocetvNemocnici();
            int zdravyP = (virusSimulation.agentOsoby().getPocetLudi()) - (mrtvyP + vyliecenyP + infekcnyP + nakazenyP);
            aktualnaReplikccia.setText(simulation.currentReplication()+"");
            String infoZdravie = (
            "Zdravy->    " + zdravyP + "\n"+
            "Mrtvy->     " + mrtvyP + "\n"+
            "Vylieceny-> " + vyliecenyP + "\n"+
            "Infekcny->  " + infekcnyP + "\n"+
            "Nemocnica-> " + nemocnicaP + "\n"+
            "Karantena-> " + testovanyP + "\n"
            );
            infoPoctyZdravie.setText(infoZdravie);

            String infoOblasti = (
            "Doma->       " + virusSimulation.agentResidencii().pocetLudiDoma() + "\n"+
            "Praca->      " + virusSimulation.agentPrace().pocetLudiVPraci() + "\n"+
            "Skola->      " + virusSimulation.agentSkoly().pocetLudiVSkolach() + "\n"+
            "Stredisko->  " + virusSimulation.agentNemocnice().pocetLudiVZariadeniach() + "\n"+
            "Centrum->    " + virusSimulation.agentCentrum().pocetLudiVCentrach() + "\n"+
            "Preprava->   " + virusSimulation.agentPresunov().pocetLudiVPreprave() + "\n"
            );
            infoPoctyMiesta.setText(infoOblasti);

            cas.setText(TimeConverter.convertToDate(simulation.currentTime()));
            if (simulation.currentTime() % GlobalVariables.DEN == 0) {
                String den = (simulation.currentTime() / GlobalVariables.DEN) + "";
                nakazenySeries.getData().add(new XYChart.Data<>(den, nakazenyP));
                infeklcnySeries.getData().add(new XYChart.Data<>(den, infekcnyP));
                vyliecenySeries.getData().add(new XYChart.Data<>(den, vyliecenyP));
                mrtvySeries.getData().add(new XYChart.Data<>(den, mrtvyP));
                karantenaSeries.getData().add(new XYChart.Data<>(den, testovanyP));
                nemocnicaSeries.getData().add(new XYChart.Data<>(den, nemocnicaP));
            }
        });

    }

    public void onSimulationWillStart(Simulation sim) {
        if (turbo.isSelected()) {
            sim.setMaxSimSpeed();
        } else {
            virusSimulation.setSimSpeed(30*rychlost.getValue(), 5.0 / rychlost.getValue());
        }
    }

    public void spocitajStatistiky() {
        double progress = (virusSimulation.currentReplication() + 1) / Double.parseDouble(pocetReplikacii.getText());
        replicationProgress.setProgress(progress);
        dniPandemie.addSample(virusSimulation.currentTime() / GlobalVariables.DEN);
        priemVylieceny.addSample(virusSimulation.agentInfekcie().pocetVylieceny());
        priemMrtvy.addSample(virusSimulation.agentInfekcie().pocetMrtvy());
        double[] inf = virusSimulation.agentInfekcie().getVrcholInfekcie();
        vrcholInfekcie.addSample(inf[0]);
        casVrcholInfekcie.addSample(inf[1] / GlobalVariables.DEN);
        double[] kar = virusSimulation.agentInfekcie().getVrcholKarantena();
        vrcholKarantena.addSample(kar[0]);
        casVrcholKarantena.addSample(kar[1] / GlobalVariables.DEN);
        double[] nem = virusSimulation.agentInfekcie().getVrcholNemocnica();
        vrcholNemocnica.addSample(nem[0]);
        casVrcholNemocnica.addSample(nem[1] / GlobalVariables.DEN);
        if (!turbo.isSelected())
            virusSimulation.setSimSpeed(30*rychlost.getValue(), 5.0 / rychlost.getValue());
    }

    public void onReplicationDidFinish(Simulation sim) {
        System.out.println("Replikacia skoncila");
        spocitajStatistiky();
    }

    public void onSimulationDidFinish(Simulation sim) {
        refresh(sim);
        sim.setSimSpeed(3600, 5.0);
        if (sim.replicationCount() > 1) {
            Platform.runLater(() -> {
                String vysledky = "";
                vysledky += "Dni pandemie            " + dniPandemie.mean() + "<" + dniPandemie.confidenceInterval_95()[0] + ";" + dniPandemie.confidenceInterval_95()[1] + ">\n";
                vysledky += "Priemerne vylieceny     " + priemVylieceny.mean() + "<" + priemVylieceny.confidenceInterval_95()[0] + ";" + priemVylieceny.confidenceInterval_95()[1] + ">\n";
                vysledky += "Priemerne mrtvy         " + priemMrtvy.mean() + "<" + priemMrtvy.confidenceInterval_95()[0] + ";" + priemMrtvy.confidenceInterval_95()[1] + ">\n";
                vysledky += "Vrchol infekcie         " + vrcholInfekcie.mean() + "<" + vrcholInfekcie.confidenceInterval_95()[0] + ";" + vrcholInfekcie.confidenceInterval_95()[1] + ">\n";
                vysledky += "Den vrcholu infekcie    " + casVrcholInfekcie.mean() + "<" + casVrcholInfekcie.confidenceInterval_95()[0] + ";" + casVrcholInfekcie.confidenceInterval_95()[1] + ">\n";
                vysledky += "Vrchol karanteny        " + vrcholKarantena.mean() + "<" + vrcholKarantena.confidenceInterval_95()[0] + ";" + vrcholKarantena.confidenceInterval_95()[1] + ">\n";
                vysledky += "Den vrcholu karanteny   " + casVrcholKarantena.mean() + "<" + casVrcholKarantena.confidenceInterval_95()[0] + ";" + casVrcholKarantena.confidenceInterval_95()[1] + ">\n";
                vysledky += "Vrchol nemocnice        " + vrcholNemocnica.mean() + "<" + vrcholNemocnica.confidenceInterval_95()[0] + ";" + vrcholNemocnica.confidenceInterval_95()[1] + ">\n";
                vysledky += "Den vrcholu nemocnice   " + casVrcholNemocnica.mean() + "<" + casVrcholNemocnica.confidenceInterval_95()[0] + ";" + casVrcholNemocnica.confidenceInterval_95()[1] + ">\n";
                infoVysledky.setText(vysledky);
                runStopButton.setText("Spusti");
            });
        }
        running = false;
        virusSimulation.stopSimulation();
    }

    public void onReplicationWillStart(Simulation sim) {
        Platform.runLater(this::setUpCharts);
    }

    public void zmenaRychlosti(int hodnota) {
        if (virusSimulation != null)
            virusSimulation.setSimSpeed(30*(double) hodnota, 5.0 / (double) hodnota);
    }
}
