package simulation;

import OSPABA.*;
import simulation.model.Osoba;

import java.util.LinkedList;

public class MyMessage extends MessageForm {

	private int kolkoOsobGenerovat;
	private Osoba osoba;
	private LinkedList<Osoba> nakazeny;

    public MyMessage(Simulation sim) {
        super(sim);
    }

    public MyMessage(MyMessage original) {
        super(original);
        // copy() is called in superclass
    }

    @Override
    public MessageForm createCopy() {
        return new MyMessage(this);
    }

    @Override
    protected void copy(MessageForm message) {
        super.copy(message);
        MyMessage original = (MyMessage) message;
        // Copy attributes
        this.kolkoOsobGenerovat = ((MyMessage) message).kolkoOsobGenerovat;
        this.osoba = ((MyMessage) message).osoba;
        this.nakazeny = ((MyMessage) message).nakazeny;
    }

	public int getKolkoOsobGenerovat() {
		return kolkoOsobGenerovat;
	}

	public void setKolkoOsobGenerovat(int kolkoOsobGenerovat) {
		this.kolkoOsobGenerovat = kolkoOsobGenerovat;
	}

	public Osoba getOsoba() {
		return osoba;
	}

	public void setOsoba(Osoba osoba) {
		this.osoba = osoba;
	}

    public LinkedList<Osoba> getNakazeny() {
        return nakazeny;
    }

    public void setNakazeny(LinkedList<Osoba> nakazeny) {
        this.nakazeny = nakazeny;
    }
}
