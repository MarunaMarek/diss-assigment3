package simulation.model;

import java.util.LinkedList;

public abstract class BoxNaOsoby {
    protected LinkedList<Osoba> osoby;

    public BoxNaOsoby() {
        osoby = new LinkedList<>();
    }

    public void pridajOsobu(Osoba osoba){
        osoby.add(osoba);
    }

    public abstract void odoberOsobu(Osoba osoba);

    public LinkedList<Osoba> getOsoby() {
        return osoby;
    }

    public void clear(){
        osoby.clear();
    }

    public int size(){
        return osoby.size();
    }
}
