package simulation.model;

public enum StavOsoby {
    NACHYLNY(0),
    NAKAZENY(1),
    INFEKCNY(2),
    TESTOVANY(3),
    NEMOCNICA(4),
    VYLIECENY(5),
    MRTVY(6);

    private final int value;

    StavOsoby(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
