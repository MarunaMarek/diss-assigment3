package simulation.model;

import OSPABA.Simulation;

public class Osoba extends OSPABA.Entity {

    private StavOsoby stav;
    private TypOsoby typOsoby;
    private int rezidencnaOblastMeno;
    private double odchodZDomu;
    private double trvanieCesty;
    private boolean ideDomov;
    private boolean zamestnany;
    private boolean cestujeHromadne;
    private int dopravnyProstriedok;
    private int pracovisko;
    private int skola;

    public Osoba(Simulation mySim, double odchodZDomu, double trvanieCesty, int rezidencnaOblastMeno,
                 TypOsoby typOsoby, StavOsoby stav, boolean zamestnany, boolean cestujeHromadne,
                 int pracovisko, int skola) {
        super(mySim);
        this.odchodZDomu = odchodZDomu - trvanieCesty;
        this.trvanieCesty = trvanieCesty;
        this.rezidencnaOblastMeno = rezidencnaOblastMeno;
        this.typOsoby = typOsoby;
        this.stav = stav;
        this.zamestnany = zamestnany;
        this.cestujeHromadne = cestujeHromadne;
        this.dopravnyProstriedok = -1;
        this.pracovisko = pracovisko;
        this.skola = skola;
    }

    public Osoba(int id, Simulation mySim) {
        super(id, mySim);
    }

    public StavOsoby getStav() {
        return stav;
    }

    public void setStav(StavOsoby stav) {

        if (this.stav.getValue() > stav.getValue()) {
            System.out.println("WARNING");
        }
        this.stav = stav;
    }

    public int getRezidencnaOblastMeno() {
        return rezidencnaOblastMeno;
    }

    public double getOdchodZDomu() {
        return odchodZDomu;
    }

    public double getTrvanieCesty() {
        return trvanieCesty;
    }

    public boolean isIdeDomov() {
        return ideDomov;
    }

    public void setIdeDomov(boolean ideDomov) {
        this.ideDomov = ideDomov;
    }

    public TypOsoby getTypOsoby() {
        return typOsoby;
    }

    public boolean jeZamestnany() {
        return zamestnany;
    }

    public boolean cestujeHromadne() {
        return cestujeHromadne;
    }

    public int getDopravnyProstriedok() {
        return dopravnyProstriedok;
    }

    public void setDopravnyProstriedok(int dopravnyProstriedok) {
        this.dopravnyProstriedok = dopravnyProstriedok;
    }

    public int getSkola() {
        return skola;
    }

    public int getPracovisko() {
        return pracovisko;
    }
}
