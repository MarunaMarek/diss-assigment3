package simulation;

import OSPABA.*;

public class Id extends IdList
{
	//meta! userInfo="Generated code: do not modify", tag="begin"
	public static final int agentModelu = 1;
	public static final int agentMesta = 3;
	public static final int agentMiest = 4;
	public static final int agentZdravia = 5;
	public static final int agentOsoby = 8;
	public static final int agentPresunov = 9;
	public static final int agentResidencii = 10;
	public static final int agentVylieceny = 11;
	public static final int agentMrtvy = 12;
	public static final int agentPrace = 24;
	public static final int agentSkoly = 25;
	public static final int agentCentrum = 26;
	public static final int agentNemocnice = 27;
	public static final int agentInfekcie = 13;
	public static final int agentNakazeny = 14;
	public static final int managerModelu = 101;
	public static final int managerMesta = 103;
	public static final int managerMiest = 104;
	public static final int managerZdravia = 105;
	public static final int managerOsoby = 108;
	public static final int managerPresunov = 109;
	public static final int managerResidencii = 110;
	public static final int managerVylieceny = 111;
	public static final int managerMrtvy = 112;
	public static final int managerPrace = 124;
	public static final int managerSkoly = 125;
	public static final int managerCentrum = 126;
	public static final int managerNemocnice = 127;
	public static final int managerInfekcie = 113;
	public static final int managerNakazeny = 114;
	public static final int kontrola = 1008;
	public static final int inkubacnaDoba = 1009;
	public static final int priebehChoroby = 1010;
	public static final int cakajDoDalsiehoDna = 1001;
	public static final int presunDopravnymProstriedkom = 1002;
	public static final int navstevaPrace = 1004;
	public static final int navstevaSkoly = 1005;
	public static final int navstevaNemocnice = 1006;
	public static final int navstevaCentra = 1007;
	public static final int dennaRutina = 1011;
	public static final int testovanie = 1012;
	public static final int vaznePriznaky = 1013;
	//meta! tag="end"
}
