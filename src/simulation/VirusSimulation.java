package simulation;

import OSPABA.*;
import agents.*;

public class VirusSimulation extends Simulation {
    public VirusSimulation(int pocetDospely, int pocetStary, int pocetDeti, int pocetPracovisk, int pocetRezidencii,
                           int pocetSkol, int pocetDopravnychProstriedkov, int kolkoTestovat, int pocetCentier,
                           int pocetZariadeni, boolean ruska, boolean zakazStary, boolean zakazDeti,
                           boolean zakazZamestnany, boolean zakazNezamestnany, double ruskaEfektivita) {
        init(pocetDospely, pocetStary, pocetDeti ,pocetPracovisk, pocetRezidencii, pocetSkol, pocetDopravnychProstriedkov,
                kolkoTestovat, pocetCentier, pocetZariadeni, ruska, zakazStary, zakazDeti, zakazZamestnany, zakazNezamestnany, ruskaEfektivita);
    }

    @Override
    public void prepareSimulation() {
        super.prepareSimulation();
        // Create global statistcis
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Reset entities, queues, local statistics, etc...
        agentModelu().spustiSimulaciu();
    }

    @Override
    public void replicationFinished() {
        // Collect local statistics into global, update UI, etc...
        super.replicationFinished();
    }

    @Override
    public void simulationFinished() {
        // Display simulation results
        super.simulationFinished();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void init(int pocetDospely, int pocetStary, int pocetDeti, int pocetPracovisk, int pocetRezidencii,
                      int pocetSkol, int pocetDopravnychProstriedkov, int kolkoTestovat, int pocetCentier,
                      int pocetZariadeni, boolean ruska, boolean zakazStary, boolean zakazDeti,
                      boolean zakazZamestnany, boolean zakazNezamestnany, double ruskaEfektivita) {
        setAgentModelu(new AgentModelu(Id.agentModelu, this, null));
        setAgentMesta(new AgentMesta(Id.agentMesta, this, agentModelu()));
        setAgentMiest(new AgentMiest(Id.agentMiest, this, agentMesta()));
        setAgentZdravia(new AgentZdravia(Id.agentZdravia, this, agentModelu()));
        setAgentOsoby(new AgentOsoby(Id.agentOsoby, this, agentMesta(), pocetPracovisk, pocetRezidencii, pocetSkol, pocetDospely, pocetStary, pocetDeti));
        setAgentPresunov(new AgentPresunov(Id.agentPresunov, this, agentMesta(), pocetDopravnychProstriedkov, ruska, ruskaEfektivita));
        setAgentResidencii(new AgentRezidencii(Id.agentResidencii, this, agentMesta(), pocetRezidencii, kolkoTestovat, zakazStary, zakazDeti, zakazZamestnany, zakazNezamestnany));
        setAgentPrace(new AgentPrace(Id.agentPrace, this, agentMiest(), pocetPracovisk, ruska, ruskaEfektivita));
        setAgentSkoly(new AgentSkoly(Id.agentSkoly, this, agentMiest(), pocetSkol, ruska, ruskaEfektivita));
        setAgentCentrum(new AgentCentrum(Id.agentCentrum, this, agentMiest(), pocetCentier, ruska, ruskaEfektivita));
        setAgentNemocnice(new AgentNemocnice(Id.agentNemocnice, this, agentMiest(), pocetZariadeni, ruska, ruskaEfektivita));
        setAgentInfekcie(new AgentInfekcie(Id.agentInfekcie, this, agentZdravia()));
        setAgentNakazeny(new AgentNakazeny(Id.agentNakazeny, this, agentZdravia()));
    }

    private AgentModelu _agentModelu;

    public AgentModelu agentModelu() {
        return _agentModelu;
    }

    public void setAgentModelu(AgentModelu agentModelu) {
        _agentModelu = agentModelu;
    }

    private AgentMesta _agentMesta;

    public AgentMesta agentMesta() {
        return _agentMesta;
    }

    public void setAgentMesta(AgentMesta agentMesta) {
        _agentMesta = agentMesta;
    }

    private AgentMiest _agentMiest;

    public AgentMiest agentMiest() {
        return _agentMiest;
    }

    public void setAgentMiest(AgentMiest agentMiest) {
        _agentMiest = agentMiest;
    }

    private AgentZdravia _agentZdravia;

    public AgentZdravia agentZdravia() {
        return _agentZdravia;
    }

    public void setAgentZdravia(AgentZdravia agentZdravia) {
        _agentZdravia = agentZdravia;
    }

    private AgentOsoby _agentOsoby;

    public AgentOsoby agentOsoby() {
        return _agentOsoby;
    }

    public void setAgentOsoby(AgentOsoby agentOsoby) {
        _agentOsoby = agentOsoby;
    }

    private AgentPresunov _agentPresunov;

    public AgentPresunov agentPresunov() {
        return _agentPresunov;
    }

    public void setAgentPresunov(AgentPresunov agentPresunov) {
        _agentPresunov = agentPresunov;
    }

    private AgentRezidencii _agentResidencii;

    public AgentRezidencii agentResidencii() {
        return _agentResidencii;
    }

    public void setAgentResidencii(AgentRezidencii agentResidencii) {
        _agentResidencii = agentResidencii;
    }

    private AgentPrace _agentPrace;

    public AgentPrace agentPrace() {
        return _agentPrace;
    }

    public void setAgentPrace(AgentPrace agentPrace) {
        _agentPrace = agentPrace;
    }

    private AgentSkoly _agentSkoly;

    public AgentSkoly agentSkoly() {
        return _agentSkoly;
    }

    public void setAgentSkoly(AgentSkoly agentSkoly) {
        _agentSkoly = agentSkoly;
    }

    private AgentCentrum _agentCentrum;

    public AgentCentrum agentCentrum() {
        return _agentCentrum;
    }

    public void setAgentCentrum(AgentCentrum agentCentrum) {
        _agentCentrum = agentCentrum;
    }

    private AgentNemocnice _agentNemocnice;

    public AgentNemocnice agentNemocnice() {
        return _agentNemocnice;
    }

    public void setAgentNemocnice(AgentNemocnice agentNemocnice) {
        _agentNemocnice = agentNemocnice;
    }

    private AgentInfekcie _agentInfekcie;

    public AgentInfekcie agentInfekcie() {
        return _agentInfekcie;
    }

    public void setAgentInfekcie(AgentInfekcie agentInfekcie) {
        _agentInfekcie = agentInfekcie;
    }

    private AgentNakazeny _agentNakazeny;

    public AgentNakazeny agentNakazeny() {
        return _agentNakazeny;
    }

    public void setAgentNakazeny(AgentNakazeny agentNakazeny) {
        _agentNakazeny = agentNakazeny;
    }
    //meta! tag="end"
}
