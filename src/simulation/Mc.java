package simulation;

import OSPABA.*;

public class Mc extends IdList
{
	//meta! userInfo="Generated code: do not modify", tag="begin"
	public static final int generujOsoby = 1001;
	public static final int ubytujOsobu = 1003;
	public static final int umiestniOsobu = 1004;
	public static final int presunOsobu = 1006;
	public static final int vykonajKontrolu = 1010;
	public static final int koniecKontroly = 1016;
	public static final int zmenaStavu = 1023;
	public static final int koniecUmiestnenia = 1031;
	public static final int umiestniPraca = 1038;
	public static final int umiestniSkola = 1040;
	public static final int umiestniNemocnica = 1042;
	public static final int umiestniCentrum = 1044;
	//meta! tag="end"

	// 1..1000 range reserved for user
	public static final int koniecCakaniaDoDalsiehoDna = 1;
	public static final int koniecPresunu = 2;
	public static final int koniecVyucovania = 3;
	public static final int koniecCentra = 4;
	public static final int koniecPrace = 5;
	public static final int osobyVygenerovane = 6;
	public static final int koniecRutiny = 7;
	public static final int koniecNemocnice = 8;
	public static final int koniecKontrolyZdravia = 9;
	public static final int zacniKontrolovat = 10;
	public static final int otestujOsoby = 11;
	public static final int koniecTestovania = 12;
	public static final int koniecInkubacnejDoby = 13;
	public static final int koniecChoroby = 14;
}
