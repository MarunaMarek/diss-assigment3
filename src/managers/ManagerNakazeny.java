package managers;

import OSPABA.*;
import simulation.*;
import agents.*;
import continualAssistants.*;
import simulation.model.Osoba;
import simulation.model.StavOsoby;

//meta! id="23"
public class ManagerNakazeny extends Manager {
    public ManagerNakazeny(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="InkubacnaDoba", id="134", type="Finish"
    public void processFinish(MessageForm message) {
        myAgent().odoberNakazeneho(((MyMessage)message).getOsoba());
        ((MyMessage)message).getOsoba().setStav(StavOsoby.INFEKCNY);
        message.setAddressee(Id.agentZdravia);
        message.setCode(Mc.zmenaStavu);
        notice(message);
    }

    //meta! sender="AgentZdravia", id="60", type="Notice"
    public void processKoniecKontroly(MessageForm message) {
        MyMessage msg = (MyMessage) message;
        for (Osoba osoba : msg.getNakazeny()) {
            MyMessage pomMsg = new MyMessage(mySim());
            pomMsg.setOsoba(osoba);
            osoba.setStav(StavOsoby.NAKAZENY);
            myAgent().pridajNakazeneho(osoba);
            pomMsg.setAddressee(myAgent().findAssistant(Id.inkubacnaDoba));
            startContinualAssistant(pomMsg);
        }
        msg.setNakazeny(null);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecKontroly:
                processKoniecKontroly(message);
                break;

            case Mc.finish:
                processFinish(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentNakazeny myAgent() {
        return (AgentNakazeny) super.myAgent();
    }

}
