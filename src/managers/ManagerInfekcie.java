package managers;

import OSPABA.*;
import simulation.*;
import agents.*;
import continualAssistants.*;
import simulation.model.Osoba;
import simulation.model.StavOsoby;

//meta! id="22"
public class ManagerInfekcie extends Manager {
    public ManagerInfekcie(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="RozvinutieVaznychPriznakov", id="137", type="Finish"
    public void processFinishChoroba(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        double zotavenia = myAgent().getSancaZotavenia();
        double sancaPodlaTypuOsoby = 0.0;
        switch (osoba.getTypOsoby()) {
            case DIETA:
                sancaPodlaTypuOsoby = GlobalVariables.SANCA_ZOTAVENIE_DETI;
                break;
            case DOSPELY:
                sancaPodlaTypuOsoby = GlobalVariables.SANCA_ZOTAVENIE_DOSPELY;
                break;
            case STARY:
                sancaPodlaTypuOsoby = GlobalVariables.SANCA_ZOTAVENIE_STARY;
                break;
        }
        if (myAgent().jeVNemocnici(osoba) && zotavenia > sancaPodlaTypuOsoby) {
            myAgent().odoberZNemocnice(osoba);
            osoba.setStav(StavOsoby.MRTVY);
            myAgent().pridajUmrtie(osoba);
        } else {
            if (myAgent().jeVNemocnici(osoba)) myAgent().odoberZNemocnice(osoba);
            if (myAgent().jeTestovany(osoba)) myAgent().odoberTestovaneho(osoba);
            if (myAgent().jeInfekcny(osoba)) myAgent().odoberInfekcneho(osoba);
            osoba.setStav(StavOsoby.VYLIECENY);
            myAgent().pridajVylieceneho(osoba);
        }
    }

    public void processFinishVaznePriznaky(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        if (osoba.getStav() != StavOsoby.VYLIECENY) {
            double vaznePriznaky = myAgent().getSancaNakaza();
            double sancaPodlaTypuOsoby = 0.0;
            switch (osoba.getTypOsoby()) {
                case DIETA:
                    sancaPodlaTypuOsoby = GlobalVariables.SANCA_VAZNY_PRIEBEH_DETI;
                    break;
                case DOSPELY:
                    sancaPodlaTypuOsoby = GlobalVariables.SANCA_VAZNY_PRIEBEH_DOSPELY;
                    break;
                case STARY:
                    sancaPodlaTypuOsoby = GlobalVariables.SANCA_VAZNY_PRIEBEH_STARY;
                    break;
            }
            if (vaznePriznaky < sancaPodlaTypuOsoby) {
                if (myAgent().jeInfekcny(osoba)) myAgent().odoberInfekcneho(osoba);
                if (myAgent().jeTestovany(osoba)) myAgent().odoberTestovaneho(osoba);
                osoba.setStav(StavOsoby.NEMOCNICA);
                myAgent().pridajDoNemocnice(osoba);
            }
        }
    }

    //meta! sender="AgentZdravia", id="62", type="Notice"
    public void processZmenaStavu(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        MessageForm kopia = message.createCopy();
        if (osoba.getStav() == StavOsoby.TESTOVANY) {
            if (myAgent().jeInfekcny(osoba)) myAgent().odoberInfekcneho(osoba);
            myAgent().pridajTestovaneho(osoba);
        } else {
            myAgent().pridajInfekcneho(osoba);
            message.setAddressee(myAgent().findAssistant(Id.priebehChoroby));
            startContinualAssistant(message);
            kopia.setAddressee(myAgent().findAssistant(Id.vaznePriznaky));
            startContinualAssistant(kopia);
        }
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.zmenaStavu:
                processZmenaStavu(message);
                break;

            case Mc.finish:
                switch (message.sender().id()) {
                    case Id.priebehChoroby:
                        processFinishChoroba(message);
                        break;
                    case Id.vaznePriznaky:
                        processFinishVaznePriznaky(message);
                        break;
                }
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentInfekcie myAgent() {
        return (AgentInfekcie) super.myAgent();
    }

}
