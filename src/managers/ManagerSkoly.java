package managers;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agents.AgentSkoly;
import simulation.GlobalVariables;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;
import simulation.model.Osoba;
import simulation.model.Skola;
import simulation.model.StavOsoby;

import java.util.LinkedList;

//meta! id="83"
public class ManagerSkoly extends Manager {
    public ManagerSkoly(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="NavstevaSkoly", id="120", type="Finish"
    public void processFinish(MessageForm message) {
        myAgent().odoberOsobu(((MyMessage)message).getOsoba());
    	message.setAddressee(Id.agentMiest);
    	message.setCode(Mc.koniecUmiestnenia);
    	notice(message);
    }

    //meta! sender="AgentMiest", id="100", type="Notice"
    public void processUmiestniSkola(MessageForm message) {
        myAgent().pridajOsobu(((MyMessage)message).getOsoba());
    	message.setAddressee(myAgent().findAssistant(Id.navstevaSkoly));
    	startContinualAssistant(message);
    }

    //meta! sender="AgentMiest", id="92", type="Notice"
    public void processKotrolaSkola(MessageForm message) {
        LinkedList<Osoba> nakazeny = new LinkedList<>();
        for(Skola skola: myAgent().getSkoly()){
            double sm = 0.0;
            double im = 0.0;
            for(Osoba osoba: skola.getOsoby()){
                if(osoba.getStav()== StavOsoby.NACHYLNY || osoba.getStav()== StavOsoby.NAKAZENY){
                    sm+=1;
                }
                if(osoba.getStav() == StavOsoby.INFEKCNY){
                    im+=1;
                }
            }
            double nm = sm+im;
            double nakazenieJedneho = ((GlobalVariables.BETA_SKOLA/GlobalVariables.T)*im*sm/nm)/sm;
            if(myAgent().isRuska()) nakazenieJedneho = nakazenieJedneho*(1.0-myAgent().getRuskaEfektivita());
            for(Osoba osoba: skola.getOsoby()){
                if(myAgent().sancaNakazenia()<nakazenieJedneho && osoba.getStav()== StavOsoby.NACHYLNY){
                    nakazeny.add(osoba);
                }
            }
        }
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.koniecKontroly);
        ((MyMessage)message).setNakazeny(nakazeny);
        notice(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        message.code();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.vykonajKontrolu:
                processKotrolaSkola(message);
                break;

            case Mc.finish:
                processFinish(message);
                break;

            case Mc.umiestniSkola:
                processUmiestniSkola(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentSkoly myAgent() {
        return (AgentSkoly) super.myAgent();
    }

}
