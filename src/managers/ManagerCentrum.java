package managers;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agents.AgentCentrum;
import simulation.GlobalVariables;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;
import simulation.model.Centrum;
import simulation.model.Osoba;
import simulation.model.StavOsoby;

import java.util.LinkedList;

//meta! id="84"
public class ManagerCentrum extends Manager {
    public ManagerCentrum(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentMiest", id="104", type="Notice"
    public void processUmiestniCentrum(MessageForm message) {
        myAgent().pridajOsosbu(((MyMessage)message).getOsoba());
		message.setAddressee(myAgent().findAssistant(Id.navstevaCentra));
		startContinualAssistant(message);
    }

    //meta! sender="NavstevaCentra", id="126", type="Finish"
    public void processFinish(MessageForm message) {
        myAgent().odoberOsobu(((MyMessage)message).getOsoba());
		message.setAddressee(Id.agentMiest);
		message.setCode(Mc.koniecUmiestnenia);
		notice(message);
    }

    //meta! sender="AgentMiest", id="96", type="Notice"
    public void processKontrolaCentrum(MessageForm message) {
        LinkedList<Osoba> nakazeny = new LinkedList<>();
        for(Centrum centrum: myAgent().getCentra()){
            double sm = 0.0;
            double im = 0.0;
            for(Osoba osoba: centrum.getOsoby()){
                if(osoba.getStav()== StavOsoby.NACHYLNY || osoba.getStav()== StavOsoby.NAKAZENY){
                    sm+=1;
                }
                if(osoba.getStav() == StavOsoby.INFEKCNY){
                    im+=1;
                }
            }
            double nm = sm+im;
            double nakazenieJedneho = ((GlobalVariables.BETA_CENTRA/GlobalVariables.T)*im*sm/nm)/sm;
            if(myAgent().isRuska()) nakazenieJedneho = nakazenieJedneho*(1.0-myAgent().getRuskaEfektivita());
            for(Osoba osoba: centrum.getOsoby()){
                if(myAgent().sancaNakazenia()<nakazenieJedneho && osoba.getStav()== StavOsoby.NACHYLNY){
                    nakazeny.add(osoba);
                }
            }
        }
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.koniecKontroly);
        ((MyMessage)message).setNakazeny(nakazeny);
        notice(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        message.code();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.vykonajKontrolu:
                processKontrolaCentrum(message);
                break;

            case Mc.umiestniCentrum:
                processUmiestniCentrum(message);
                break;

            case Mc.finish:
                processFinish(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentCentrum myAgent() {
        return (AgentCentrum) super.myAgent();
    }

}
