package managers;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agents.AgentPresunov;
import simulation.GlobalVariables;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;
import simulation.model.HromadnyDopravnyProstriedok;
import simulation.model.Osoba;
import simulation.model.StavOsoby;

import java.util.LinkedList;

//meta! id="12"
public class ManagerPresunov extends Manager {
    public ManagerPresunov(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentMesta", id="43", type="Notice"
    public void processPresunOsobu(MessageForm message) {
        Osoba osoba = ((MyMessage)message).getOsoba();
        if(osoba.cestujeHromadne()){
            myAgent().pridajOsobuDoHromadnejPrepravy(osoba);
        }else {
            myAgent().pridajOsobuDoOsobnejPrepravy(osoba);
        }
    	message.setAddressee(myAgent().findAssistant(Id.presunDopravnymProstriedkom));
    	startContinualAssistant(message);
    }

    //meta! sender="AgentMesta", id="50", type="Notice"
    public void processVykonajKontrolu(MessageForm message) {
        LinkedList<Osoba> nakazeny = new LinkedList<>();
        for(HromadnyDopravnyProstriedok prostriedok: myAgent().getDopravneProstriedky()){
            double sm = 0.0;
            double im = 0.0;
            for(Osoba osoba: prostriedok.getOsoby()){
                if(osoba.getStav()== StavOsoby.NACHYLNY || osoba.getStav()== StavOsoby.NAKAZENY){
                    sm+=1;
                }
                if(osoba.getStav() == StavOsoby.INFEKCNY){
                    im+=1;
                }
            }
            double nm = sm+im;
            double nakazenieJedneho = ((GlobalVariables.BETA_DOPRAVNE_PROSTRIEDKY/GlobalVariables.T)*im*sm/nm)/sm;
            if(myAgent().isRuska()) nakazenieJedneho = nakazenieJedneho*(1.0-myAgent().getRuskaEfektivita());
            for(Osoba osoba: prostriedok.getOsoby()){
                if(myAgent().sancaNakazenia()<nakazenieJedneho && osoba.getStav()== StavOsoby.NACHYLNY){
                    nakazeny.add(osoba);
                }
            }
        }
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.koniecKontroly);
        ((MyMessage)message).setNakazeny(nakazeny);
        notice(message);
    }

    //meta! sender="PresunDopravnymProstriedkom", id="112", type="Finish"
    public void processFinish(MessageForm message) {
        Osoba osoba = ((MyMessage)message).getOsoba();
        if(osoba.cestujeHromadne()){
            myAgent().odoberOsobuZHromadnejPrepravy(osoba);
        }else {
            myAgent().odoberOsobuZOsobnejPrepravy(osoba);
        }
    	message.setAddressee(Id.agentMesta);
		message.setCode(Mc.umiestniOsobu);
		notice(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        message.code();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.presunOsobu:
                processPresunOsobu(message);
                break;

            case Mc.vykonajKontrolu:
                processVykonajKontrolu(message);
                break;

            case Mc.finish:
                processFinish(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentPresunov myAgent() {
        return (AgentPresunov) super.myAgent();
    }

}
