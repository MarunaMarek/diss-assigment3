package managers;

import OSPABA.*;
import simulation.*;
import agents.*;
import simulation.model.Osoba;
import simulation.model.RezidencnaOblast;
import simulation.model.StavOsoby;
import simulation.model.TypOsoby;

import java.util.LinkedList;

//meta! id="13"
public class ManagerRezidencii extends Manager {
    public ManagerRezidencii(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentMesta", id="51", type="Notice"
    public void processVykonajKontrolu(MessageForm message) {
        LinkedList<Osoba> nakazeny = new LinkedList<>();
        for (RezidencnaOblast ro : myAgent().getRezidencneOblasti()) {
            double sm = 0.0;
            double im = 0.0;
            for (Osoba osoba : ro.getOsoby()) {
                if (osoba.getStav() == StavOsoby.NACHYLNY || osoba.getStav() == StavOsoby.NAKAZENY) {
                    sm += 1;
                }
                if (osoba.getStav() == StavOsoby.INFEKCNY) {
                    im += 1;
                }
            }
            double nm = sm + im;
            double nakazenieJedneho = ((GlobalVariables.BETA_REZIDENCIA / GlobalVariables.T) * im * sm / nm) / sm;
            for (Osoba osoba : ro.getOsoby()) {
                if (myAgent().sancaNakazenia() < nakazenieJedneho && osoba.getStav() == StavOsoby.NACHYLNY) {
                    nakazeny.add(osoba);
                }
            }
        }
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.koniecKontroly);
        ((MyMessage) message).setNakazeny(nakazeny);
        notice(message);
    }

    public void processOtestujOsoby(MessageForm message) {
        LinkedList<Osoba> vhodneNaTestovanie = new LinkedList<>();
        LinkedList<Osoba> pozitivny = new LinkedList<>();
        int pocetTestov = myAgent().getKolkoTestovat();
        for (Osoba os : myAgent().getOsoby()) {
            if (os.getStav() == StavOsoby.NACHYLNY || os.getStav() == StavOsoby.NAKAZENY || os.getStav() == StavOsoby.INFEKCNY) {
                vhodneNaTestovanie.add(os);
            }
        }
        double sancaTestu = (double) pocetTestov / (double) vhodneNaTestovanie.size();
        for (Osoba os : vhodneNaTestovanie) {
            if (myAgent().sancaTestovanie() < sancaTestu) {
                if (os.getStav() == StavOsoby.INFEKCNY) {
                    os.setStav(StavOsoby.TESTOVANY);
                    pozitivny.add(os);
                }
            }
        }

        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.koniecTestovania);
        ((MyMessage) message).setNakazeny(pozitivny);
        notice(message);
    }

    //meta! sender="CakajDoDalsiehoDna", id="109", type="Finish"
    public void processFinish(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        if (osoba.getStav() == StavOsoby.MRTVY) {
            switch (osoba.getTypOsoby()) {
                case DIETA:
                    myAgent().getDeti().remove(osoba);
                    myAgent().getMrtvy().add(osoba);
                    break;
                case DOSPELY:
                    if (!osoba.jeZamestnany()) {
                        myAgent().getNezamestnany().remove(osoba);
                        myAgent().getMrtvy().add(osoba);
                    } else {
                        myAgent().getZamestnany().remove(osoba);
                        myAgent().getMrtvy().add(osoba);
                    }
                    break;
                case STARY:
                    myAgent().getStary().remove(osoba);
                    myAgent().getMrtvy().add(osoba);
                    break;
            }
            myAgent().odoberOsobuZRezidencie(osoba);
            return;
        }
        if (osoba.getStav() == StavOsoby.TESTOVANY || osoba.getStav() == StavOsoby.NEMOCNICA) {
            switch (osoba.getTypOsoby()) {
                case DIETA:
                    myAgent().getDeti().remove(osoba);
                    myAgent().getKarantena().add(osoba);
                    break;
                case DOSPELY:
                    if (!osoba.jeZamestnany()) {
                        myAgent().getNezamestnany().remove(osoba);
                        myAgent().getKarantena().add(osoba);
                    } else {
                        myAgent().getZamestnany().remove(osoba);
                        myAgent().getKarantena().add(osoba);
                    }
                    break;
                case STARY:
                    myAgent().getStary().remove(osoba);
                    myAgent().getKarantena().add(osoba);
                    break;
            }
            myAgent().odoberOsobuZRezidencie(osoba);
            return;
        }
        if (myAgent().isZakazDeti() && osoba.getTypOsoby() == TypOsoby.DIETA) return;
        if (myAgent().isZakazNezamestnany() && osoba.getTypOsoby() == TypOsoby.DOSPELY && !osoba.jeZamestnany()) return;
        if (myAgent().isZakazZamestnany() && osoba.getTypOsoby() == TypOsoby.DOSPELY && osoba.jeZamestnany()) return;
        if (myAgent().isZakazStary() && osoba.getTypOsoby() == TypOsoby.STARY) return;
        myAgent().odoberOsobuZRezidencie(osoba);
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.presunOsobu);
        notice(message);
    }

    public void processFinishDennaRutina(MessageForm message) {
        LinkedList<Osoba> odobratyZkaranteny = new LinkedList<>();
        for (Osoba os : myAgent().getKarantena()) {
            if (os.getStav() == StavOsoby.MRTVY) {
                odobratyZkaranteny.add(os);
                myAgent().getMrtvy().add(os);
            }
            if (os.getStav() == StavOsoby.VYLIECENY) {
                odobratyZkaranteny.add(os);
                myAgent().pridajOsosbuDoRezidencie(os);
                if (os.getTypOsoby() == TypOsoby.DOSPELY) {
                    if (os.jeZamestnany()) {
                        myAgent().getZamestnany().add(os);
                    } else {
                        myAgent().getNezamestnany().add(os);
                    }
                } else if (os.getTypOsoby() == TypOsoby.STARY) {
                    myAgent().getStary().add(os);
                } else {
                    myAgent().getDeti().add(os);
                    MyMessage msg = new MyMessage(mySim());
                    msg.setOsoba(os);
                    msg.setAddressee(myAgent().findAssistant(Id.cakajDoDalsiehoDna));
                    startContinualAssistant(msg);
                }
            }
        }

        myAgent().getKarantena().removeAll(odobratyZkaranteny);
        LinkedList<Osoba> nezamestnany = myAgent().getNezamestnany();
        LinkedList<Osoba> stary = myAgent().getStary();
        LinkedList<Osoba> zamestnany = myAgent().getZamestnany();

        for (Osoba os : nezamestnany) {
            if (myAgent().nezamestnanyOdchod()) {
                MyMessage msg = new MyMessage(mySim());
                msg.setOsoba(os);
                msg.setAddressee(myAgent().findAssistant(Id.cakajDoDalsiehoDna));
                startContinualAssistant(msg);
            }
        }
        for (Osoba os : stary) {
            if (myAgent().staryOdchod()) {
                MyMessage msg = new MyMessage(mySim());
                msg.setOsoba(os);
                msg.setAddressee(myAgent().findAssistant(Id.cakajDoDalsiehoDna));
                startContinualAssistant(msg);
            }
        }
        if ((Math.floor((mySim().currentTime() / GlobalVariables.DEN)) % 7) == 6 || (Math.floor((mySim().currentTime() / GlobalVariables.DEN)) % 7) == 0)
            for (Osoba os : zamestnany) {
                MyMessage msg = new MyMessage(mySim());
                msg.setOsoba(os);
                msg.setAddressee(myAgent().findAssistant(Id.cakajDoDalsiehoDna));
                startContinualAssistant(msg);
            }
        message.setAddressee(myAgent().findAssistant(Id.dennaRutina));
        startContinualAssistant(message);
    }

    //meta! sender="AgentMesta", id="41", type="Notice"
    public void processUbytujOsobu(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        myAgent().pridajOsosbuDoRezidencie(osoba);
        myAgent().pridajOsobuDoKategorie(osoba);
        if (osoba.getTypOsoby() == TypOsoby.DIETA || (osoba.getTypOsoby() == TypOsoby.DOSPELY && osoba.jeZamestnany())) {
            message.setAddressee(myAgent().findAssistant(Id.cakajDoDalsiehoDna));
            startContinualAssistant(message);
        }
    }

    public void processUmietsniOsobu(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        myAgent().pridajOsosbuDoRezidencie(osoba);
        if (osoba.getTypOsoby() == TypOsoby.DIETA || (osoba.getTypOsoby() == TypOsoby.DOSPELY && osoba.jeZamestnany())) {
            message.setAddressee(myAgent().findAssistant(Id.cakajDoDalsiehoDna));
            startContinualAssistant(message);
        }
    }

    public void processNastavScheduler(MessageForm message) {
        message.setAddressee(myAgent().findAssistant(Id.dennaRutina));
        startContinualAssistant(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.osobyVygenerovane:
                processNastavScheduler(message);
                break;
            case Mc.umiestniOsobu:
                processUmietsniOsobu(message);
                break;
            case Mc.otestujOsoby:
                processOtestujOsoby(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.vykonajKontrolu:
                processVykonajKontrolu(message);
                break;

            case Mc.ubytujOsobu:
                processUbytujOsobu(message);
                break;

            case Mc.finish:
                switch (message.sender().id()) {
                    case Id.cakajDoDalsiehoDna:
                        processFinish(message);
                        break;
                    case Id.dennaRutina:
                        processFinishDennaRutina(message);
                        break;
                }
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentRezidencii myAgent() {
        return (AgentRezidencii) super.myAgent();
    }

}
