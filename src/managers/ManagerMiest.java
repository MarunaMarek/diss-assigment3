package managers;

import OSPABA.*;
import simulation.*;
import agents.*;
import simulation.model.Osoba;

//meta! id="4"
public class ManagerMiest extends Manager {
    public ManagerMiest(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentSkoly", id="93", type="Notice"
    public void processKoniecUmiestneniaAgentSkoly(MessageForm message) {
        if (myAgent().ideDietaDoCentra()) {
            message.setAddressee(Id.agentCentrum);
            message.setCode(Mc.umiestniCentrum);
        } else {
            message.setAddressee(Id.agentMesta);
            message.setCode(Mc.presunOsobu);
        }
        notice(message);
    }

    //meta! sender="AgentCentrum", id="97", type="Notice"
    public void processKoniecUmiestneniaAgentCentrum(MessageForm message) {
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.presunOsobu);
        notice(message);
    }

    //meta! sender="AgentNemocnice", id="103", type="Notice"
    public void processKoniecUmiestneniaNemocnice(MessageForm message) {
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.presunOsobu);
        notice(message);
    }


    //meta! sender="AgentPrace", id="91", type="Notice"
    public void processKoniecUmiestneniaAgentPrace(MessageForm message) {
        if (myAgent().ideDospelyDoCentra()) {
            message.setAddressee(Id.agentCentrum);
            message.setCode(Mc.umiestniCentrum);
        } else {
            message.setAddressee(Id.agentMesta);
            message.setCode(Mc.presunOsobu);
        }
        notice(message);
    }

    //meta! sender="AgentMesta", id="40", type="Notice"
    public void processUmiestniOsobu(MessageForm message) {
        Osoba osoba = ((MyMessage) message).getOsoba();
        switch (osoba.getTypOsoby()) {
            case DIETA:
                message.setAddressee(Id.agentSkoly);
                message.setCode(Mc.umiestniSkola);
                notice(message);
                break;
            case DOSPELY:
                if (!osoba.jeZamestnany()) {
                    if (myAgent().ideDoNemocnice()) {
                        message.setAddressee(Id.agentNemocnice);
                        message.setCode(Mc.umiestniNemocnica);
                    } else {
                        message.setAddressee(Id.agentCentrum);
                        message.setCode(Mc.umiestniCentrum);
                    }
                } else {
                    if (((mySim().currentTime() % GlobalVariables.DEN) / GlobalVariables.HODINA) < 20.0) {
                        message.setAddressee(Id.agentPrace);
                        message.setCode(Mc.umiestniPraca);
                    } else {
                        message.setAddressee(Id.agentCentrum);
                        message.setCode(Mc.umiestniCentrum);
                    }
                }
                notice(message);
                break;
            case STARY:
                if (myAgent().ideDoNemocnice()) {
                    message.setAddressee(Id.agentNemocnice);
                    message.setCode(Mc.umiestniNemocnica);
                } else {
                    message.setAddressee(Id.agentCentrum);
                    message.setCode(Mc.umiestniCentrum);
                }
                notice(message);
                break;
        }
    }

    //meta! sender="AgentMesta", id="49", type="Notice"
    public void processVykonajKontrolu(MessageForm message) {
        MessageForm kopia1 = message.createCopy();
        MessageForm kopia2 = message.createCopy();
        MessageForm kopia3 = message.createCopy();
        MessageForm kopia4 = message.createCopy();
        kopia1.setAddressee(Id.agentPrace);
        kopia2.setAddressee(Id.agentSkoly);
        kopia3.setAddressee(Id.agentNemocnice);
        kopia4.setAddressee(Id.agentCentrum);
        notice(kopia1);
        notice(kopia2);
        notice(kopia3);
        notice(kopia4);
    }

    //meta! sender="AgentNemocnice", id="95", type="Notice"
    public void processKoniecKontroly(MessageForm message) {
        message.setAddressee(Id.agentMiest);
        message.setCode(Mc.koniecKontroly);
        notice(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        message.code();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecUmiestnenia:
                switch (message.sender().id()) {
                    case Id.agentSkoly:
                        processKoniecUmiestneniaAgentSkoly(message);
                        break;

                    case Id.agentCentrum:
                        processKoniecUmiestneniaAgentCentrum(message);
                        break;

                    case Id.agentPrace:
                        processKoniecUmiestneniaAgentPrace(message);
                        break;
                    case Id.agentNemocnice:
                        processKoniecUmiestneniaNemocnice(message);
                        break;
                }
                break;

            case Mc.koniecKontroly:
                processKoniecKontroly(message);
                break;

            case Mc.vykonajKontrolu:
                processVykonajKontrolu(message);
                break;

            case Mc.umiestniOsobu:
                processUmiestniOsobu(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentMiest myAgent() {
        return (AgentMiest) super.myAgent();
    }
}
