package managers;

import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="1"
public class ManagerModelu extends Manager {
    public ManagerModelu(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentZdravia", id="46", type="Notice"
    public void processVykonajKontrolu(MessageForm message) {
        message.setAddressee(Id.agentMesta);
        notice(message);
    }

    public void processOtestujOsoby(MessageForm message) {
        message.setAddressee(Id.agentMesta);
        notice(message);
    }

    //meta! sender="AgentMesta", id="56", type="Notice"
    public void processKoniecKontroly(MessageForm message) {
        message.setAddressee(Id.agentZdravia);
        notice(message);
    }

    public void processKoniecTestovania(MessageForm message) {
        message.setAddressee(Id.agentZdravia);
        notice(message);
    }

    public void processGenerujOsoby(MessageForm message) {
    	message.setAddressee(Id.agentMesta);
    	notice(message);
    	MyMessage msg = new MyMessage(mySim());
    	msg.setAddressee(Id.agentZdravia);
    	msg.setCode(Mc.zacniKontrolovat);
    	notice(msg);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.generujOsoby:
                processGenerujOsoby(message);
                break;
            case Mc.otestujOsoby:
                processOtestujOsoby(message);
                break;
            case Mc.koniecTestovania:
                processKoniecTestovania(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.vykonajKontrolu:
                processVykonajKontrolu(message);
                break;

            case Mc.koniecKontroly:
                processKoniecKontroly(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentModelu myAgent() {
        return (AgentModelu) super.myAgent();
    }

}
