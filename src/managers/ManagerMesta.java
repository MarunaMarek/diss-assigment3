package managers;

import OSPABA.*;
import simulation.*;
import agents.*;

//meta! id="3"
public class ManagerMesta extends Manager {
    public ManagerMesta(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentPresunov", id="44", type="Notice"
    public void processUmiestniOsobu(MessageForm message) {
        MyMessage myMessage = (MyMessage)message;
        if(myMessage.getOsoba().isIdeDomov()){
            message.setAddressee(Id.agentResidencii);
            notice(message);
        }else {
            message.setAddressee(Id.agentMiest);
            notice(message);
        }
    }

    //meta! sender="AgentModelu", id="36", type="Notice"
    public void processGenerujOsoby(MessageForm message) {
    	message.setAddressee(Id.agentOsoby);
        notice(message);
    }

    //meta! sender="AgentMiest", id="45", type="Notice"
    public void processPresunOsobuAgentMiest(MessageForm message) {
        message.setAddressee(Id.agentPresunov);
        ((MyMessage)message).getOsoba().setIdeDomov(true);
        notice(message);
    }

    //meta! sender="AgentResidencii", id="42", type="Notice"
    public void processPresunOsobuAgentResidencii(MessageForm message) {
        message.setAddressee(Id.agentPresunov);
        ((MyMessage)message).getOsoba().setIdeDomov(false);
        notice(message);
    }

    //meta! sender="AgentModelu", id="47", type="Notice"
    public void processVykonajKontrolu(MessageForm message) {
        MessageForm kopia1 = message.createCopy();
        MessageForm kopia2 = message.createCopy();
        MessageForm kopia3 = message.createCopy();
        kopia1.setAddressee(Id.agentResidencii);
        kopia2.setAddressee(Id.agentPresunov);
        kopia3.setAddressee(Id.agentMiest);
        notice(kopia1);
        notice(kopia2);
        notice(kopia3);
    }

    public void processOtestujOsoby(MessageForm message) {
        message.setAddressee(Id.agentResidencii);
        notice(message);
    }

    //meta! sender="AgentOsoby", id="39", type="Notice"
    public void processUbytujOsobu(MessageForm message) {
        message.setAddressee(Id.agentResidencii);
        notice(message);
    }

    //meta! sender="AgentMiest", id="54", type="Notice"
    public void processKoniecKontroly(MessageForm message) {
        message.setAddressee(Id.agentModelu);
        notice(message);
    }

    public void processKoniecTestovania(MessageForm message) {
        message.setAddressee(Id.agentModelu);
        notice(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.osobyVygenerovane:
                message.setAddressee(Id.agentResidencii);
                notice(message);
                break;
            case Mc.otestujOsoby:
                processOtestujOsoby(message);
                break;
            case Mc.koniecTestovania:
                processKoniecTestovania(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.umiestniOsobu:
                processUmiestniOsobu(message);
                break;

            case Mc.presunOsobu:
                switch (message.sender().id()) {
                    case Id.agentMiest:
                        processPresunOsobuAgentMiest(message);
                        break;

                    case Id.agentResidencii:
                        processPresunOsobuAgentResidencii(message);
                        break;
                }
                break;

            case Mc.generujOsoby:
                processGenerujOsoby(message);
                break;

            case Mc.vykonajKontrolu:
                processVykonajKontrolu(message);
                break;

            case Mc.koniecKontroly:
                processKoniecKontroly(message);
                break;

            case Mc.ubytujOsobu:
                processUbytujOsobu(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentMesta myAgent() {
        return (AgentMesta) super.myAgent();
    }

}
