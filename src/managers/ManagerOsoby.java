package managers;

import OSPABA.*;
import simulation.*;
import agents.*;
import simulation.model.Osoba;
import simulation.model.StavOsoby;
import simulation.model.TypOsoby;

import java.util.LinkedList;

//meta! id="11"
public class ManagerOsoby extends Manager {

    public ManagerOsoby(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentMesta", id="38", type="Notice"
    public void processGenerujOsoby(MessageForm message) {
        generujOsosby((MyMessage) message);
    }

    private void generujOsosby(MyMessage message) {
        LinkedList<Osoba> nakazeny = new LinkedList<>();
        nakazeny.add(vytvorDospeleho(StavOsoby.NAKAZENY));
        nakazeny.add(vytvorStareho(StavOsoby.NAKAZENY));
        nakazeny.add(vytvorDieta(StavOsoby.NAKAZENY));
        for (int i = 0; i < myAgent().getPocetDospely(); i++) {
            if(myAgent().sancaNakazeny()<0.01){
                nakazeny.add(vytvorDospeleho(StavOsoby.NAKAZENY));
            }else {
                vytvorDospeleho(StavOsoby.NACHYLNY);
            }
        }
        for (int i = 0; i < myAgent().getPocetStary(); i++) {
            if(myAgent().sancaNakazeny()<0.01){
                nakazeny.add(vytvorStareho(StavOsoby.NAKAZENY));
            }else {
                vytvorStareho(StavOsoby.NACHYLNY);
            }
        }
        for (int i = 0; i < myAgent().getPocetDeti(); i++) {
            if(myAgent().sancaNakazeny()<0.01){
                nakazeny.add(vytvorDieta(StavOsoby.NAKAZENY));
            }else {
                vytvorDieta(StavOsoby.NACHYLNY);
            }
        }

        MyMessage msg1 = new MyMessage(mySim());
        msg1.setNakazeny(nakazeny);
        msg1.setAddressee(Id.agentMesta);
        msg1.setCode(Mc.koniecKontroly);
        notice(msg1);

        //start schedulera na dennu rutinu
        MyMessage msg = new MyMessage(mySim());
        msg.setAddressee(Id.agentMesta);
        msg.setCode(Mc.osobyVygenerovane);
        notice(msg);
    }

    private Osoba vytvorDieta(StavOsoby stavOsoby) {
        //inicializacia casu
        double casCesty = myAgent().casDochadzaniaDeti();
        //inicializacia dietata
        Osoba osoba = new Osoba(mySim(), GlobalVariables.ZACIATOK_DNA_DETI, casCesty, myAgent().rezidencnaOblast(), TypOsoby.DIETA,
                stavOsoby, false, true, -1, myAgent().nahodnaSkola());
        //inicializacia spravy
        MyMessage msg = new MyMessage(mySim());
        msg.setAddressee(Id.agentMesta);
        msg.setOsoba(osoba);
        msg.setCode(Mc.ubytujOsobu);
        notice(msg);
        return osoba;
    }

    private Osoba vytvorDospeleho(StavOsoby stavOsoby) {
        //inicializacia casu
        boolean zamesntany = myAgent().jeDospelyZamestnany();
        boolean hromadne = true;
        double casCesty;
        double zaciatokDna;
        if (zamesntany) {
            zaciatokDna = GlobalVariables.ZACIATOK_DNA_ZAMESTNANY;
            if (myAgent().cestujeDospelyHromadne()) {
                casCesty = myAgent().casDochadzaniaDospelyHromadne();
            } else {
                hromadne = false;
                casCesty = myAgent().casDochadzaniaDospelyIndividualne();
            }
        } else {
            zaciatokDna = GlobalVariables.ZACIATOK_DNA_NEZAMESTNANY;
            if (!myAgent().cestujeNezamestnanyHromadne()) hromadne = false;
            casCesty = myAgent().casDochadzaniaNezamestnany();
        }
        //inicializacia dospeleho
        Osoba osoba = new Osoba(mySim(), zaciatokDna, casCesty, myAgent().rezidencnaOblast(), TypOsoby.DOSPELY,
                stavOsoby, zamesntany, hromadne, myAgent().nahodnaPraca(), -1);
        //inicializacia spravy
        MyMessage msg = new MyMessage(mySim());
        msg.setAddressee(Id.agentMesta);
        msg.setOsoba(osoba);
        msg.setCode(Mc.ubytujOsobu);
        notice(msg);
        return osoba;
    }

    private Osoba vytvorStareho(StavOsoby stavOsoby) {
        //inicializacia casu
        boolean hromadne = myAgent().cestujeStaryHromadne();
        double casCesty;
        double zaciatokDna;

        zaciatokDna = GlobalVariables.ZACIATOK_DNA_STARY;
        casCesty = myAgent().casDochadzaniaStary();

        //inicializacia dospeleho
        Osoba osoba = new Osoba(mySim(), zaciatokDna, casCesty, myAgent().rezidencnaOblast(), TypOsoby.STARY, stavOsoby, false, hromadne, -1, -1);
        //inicializacia spravy
        MyMessage msg = new MyMessage(mySim());
        msg.setAddressee(Id.agentMesta);
        msg.setOsoba(osoba);
        msg.setCode(Mc.ubytujOsobu);
        notice(msg);
        return osoba;
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        message.code();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.generujOsoby:
                processGenerujOsoby(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentOsoby myAgent() {
        return (AgentOsoby) super.myAgent();
    }

}
