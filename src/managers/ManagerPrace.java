package managers;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agents.AgentPrace;
import simulation.GlobalVariables;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;
import simulation.model.Osoba;
import simulation.model.Pracovisko;
import simulation.model.StavOsoby;

import java.util.LinkedList;

//meta! id="82"
public class ManagerPrace extends Manager {
    public ManagerPrace(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="AgentMiest", id="98", type="Notice"
    public void processUmiestniPraca(MessageForm message) {
    	myAgent().pridajOsobu(((MyMessage)message).getOsoba());
    	message.setAddressee(myAgent().findAssistant(Id.navstevaPrace));
		startContinualAssistant(message);
    }

    //meta! sender="AgentMiest", id="90", type="Notice"
    public void processKontrolaPraca(MessageForm message) {
        LinkedList<Osoba> nakazeny = new LinkedList<>();
        for(Pracovisko pracovisko: myAgent().getPracoviska()){
            double sm = 0.0;
            double im = 0.0;
            for(Osoba osoba: pracovisko.getOsoby()){
                if(osoba.getStav()== StavOsoby.NACHYLNY || osoba.getStav()== StavOsoby.NAKAZENY){
                    sm+=1;
                }
                if(osoba.getStav() == StavOsoby.INFEKCNY){
                    im+=1;
                }
            }
            double nm = sm+im;
            double nakazenieJedneho = ((GlobalVariables.BETA_PRACA/GlobalVariables.T)*im*sm/nm)/sm;
            if(myAgent().isRuska()) nakazenieJedneho = nakazenieJedneho*(1.0-myAgent().getRuskaEfektivita());
            for(Osoba osoba: pracovisko.getOsoby()){
                if(myAgent().sancaNakazenia()<nakazenieJedneho && osoba.getStav()== StavOsoby.NACHYLNY){
                    nakazeny.add(osoba);
                }
            }
        }
        message.setAddressee(Id.agentMesta);
        message.setCode(Mc.koniecKontroly);
        ((MyMessage)message).setNakazeny(nakazeny);
        notice(message);
    }

    //meta! sender="NavstevaPrace", id="117", type="Finish"
    public void processFinish(MessageForm message) {
        myAgent().odoberOsobu(((MyMessage)message).getOsoba());
		message.setAddressee(Id.agentMiest);
		message.setCode(Mc.koniecUmiestnenia);
		notice(message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        message.code();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.umiestniPraca:
                processUmiestniPraca(message);
                break;

            case Mc.vykonajKontrolu:
                processKontrolaPraca(message);
                break;

            case Mc.finish:
                processFinish(message);
                break;

            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentPrace myAgent() {
        return (AgentPrace) super.myAgent();
    }

}
