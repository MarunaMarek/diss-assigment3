package managers;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agents.AgentZdravia;
import simulation.Id;
import simulation.Mc;
import simulation.MyMessage;
import simulation.VirusSimulation;
import simulation.model.Osoba;

import java.util.LinkedList;

//meta! id="5"
public class ManagerZdravia extends Manager {
    public ManagerZdravia(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
        init();
    }

    @Override
    public void prepareReplication() {
        super.prepareReplication();
        // Setup component for the next replication

        if (petriNet() != null) {
            petriNet().clear();
        }
    }

    //meta! sender="Kontrola", id="129", type="Finish"
    public void processFinish(MessageForm message) {
        MessageForm kopia1 = message.createCopy();
        message.setAddressee(Id.agentModelu);
        message.setCode(Mc.vykonajKontrolu);
        notice(message);
        kopia1.setAddressee(myAgent().findAssistant(Id.kontrola));
        startContinualAssistant(kopia1);
    }

    //meta! sender="Testovanie", id="129", type="Finish"
    public void processFinishTestovanie(MessageForm message) {
        VirusSimulation virusSimulation = (VirusSimulation) mySim();
        if ((virusSimulation.agentInfekcie().pocetInfekcny() + virusSimulation.agentNakazeny().pocetNakazenych() + virusSimulation.agentInfekcie().pocetTestovany() + virusSimulation.agentInfekcie().pocetvNemocnici()) == 0 && virusSimulation.currentTime() > 100) {
            virusSimulation.replicationFinished();
        } else {
            MessageForm kopia1 = message.createCopy();
            message.setAddressee(Id.agentModelu);
            message.setCode(Mc.otestujOsoby);
            notice(message);
            kopia1.setAddressee(myAgent().findAssistant(Id.testovanie));
            startContinualAssistant(kopia1);
        }
    }

    //meta! sender="AgentModelu", id="57", type="Notice"
    public void processKoniecKontroly(MessageForm message) {
        message.setAddressee(Id.agentNakazeny);
        notice(message);
    }

    public void processKoniecTestovania(MessageForm message) {
        LinkedList<Osoba> testovany = ((MyMessage) message).getNakazeny();
        for (Osoba os : testovany) {
            MyMessage myMessage = new MyMessage(mySim());
            myMessage.setOsoba(os);
            myMessage.setCode(Mc.zmenaStavu);
            notice(myMessage);
        }
    }

    //meta! sender="AgentNakazeny", id="61", type="Notice"
    public void processZmenaStavu(MessageForm message) {
        message.setAddressee(Id.agentInfekcie);
        notice(message);
    }

    public void processZacniKontrolovat(MessageForm message) {
        MessageForm kopia1 = message.createCopy();
        MessageForm kopia2 = message.createCopy();
        kopia1.setAddressee(myAgent().findAssistant(Id.kontrola));
        startContinualAssistant(kopia1);
        kopia2.setAddressee(myAgent().findAssistant(Id.testovanie));
        startContinualAssistant(kopia2);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void processDefault(MessageForm message) {
        switch (message.code()) {
            case Mc.zacniKontrolovat:
                processZacniKontrolovat(message);
                break;
            case Mc.koniecTestovania:
                processKoniecTestovania(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void init() {
    }

    @Override
    public void processMessage(MessageForm message) {
        switch (message.code()) {
            case Mc.koniecKontroly:
                processKoniecKontroly(message);
                break;

            case Mc.zmenaStavu:
                processZmenaStavu(message);
                break;

            case Mc.finish:
                switch (message.sender().id()) {
                    case Id.kontrola:
                        processFinish(message);
                        break;
                    case Id.testovanie:
                        processFinishTestovanie(message);
                        break;
                }
            default:
                processDefault(message);
                break;
        }
    }
    //meta! tag="end"

    @Override
    public AgentZdravia myAgent() {
        return (AgentZdravia) super.myAgent();
    }

}
